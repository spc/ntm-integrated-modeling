
This ntm combination of 3 modules computes the time evolution of an existing island from tn to
tn+1 following the modified Rutherfors equation

This existence of a seed island is determined in the checktearing module

The time evolution is determined in the ntmets module

The effect on transport is determined in the ntmDeffTLets module (by a simple addition of a large chi near the mode location and with a width related to the island width
