program ntm_DeffTL_progETS

  use itm_types     
  use ids_schemas
  use ids_routines

  implicit none

  interface

     subroutine  ntm_DeffTLets( cor_in,ntms_in, coretransp_in, coretransp_out,param_in)
       ! subroutine  ntm_Deff( ntms_in, coretransp_in, coretransp_out, param_in)

       use ids_schemas
       USE interpos_module

       USE itm_types

       type (ids_core_transport) ::     coretransp_in, coretransp_out
       type (ids_core_profiles) ::        cor_in
       type (ids_ntms) ::          ntms_in

       type (ids_Parameters_Input)  :: param_in         

     end subroutine ntm_DeffTLets
  end interface

  type (ids_core_transport) ::     coretransp_in, coretransp_out
  type (ids_core_transport) ::     coretransp_in_slice, coretransp_out_slice
  type (ids_core_profiles) ::          core_profiles_slice,core_profiles_all
  type (ids_ntms) ::          ntms_in
  type (ids_ntms) ::          ntms_in_slice

  type (ids_Parameters_Input) :: codeparam_param

  integer :: idxin, idxout, shot, runin, runout, refshot, refrun, idxmhd, runh
  integer :: i, INXML, ios, n_lines, jt, nb_transport_model, i_interpol

  character(len=5)::treename
  character(len = 132), allocatable :: parameters(:)
  character(len = 132) :: xml_line, tree_user, tree_tokamak

  ! shot = 11
  ! runin = 24
  !! runin = 41
  ! runout = 32
  ! refshot = 0 ! Dummy, not used
  refrun =0   ! Dummy, not used


  ! read xml input file
  !
  INXML=1
  OPEN (unit = INXML, file = "ntm_deff_inputs.xml", status = 'old', &
    action = 'read', iostat = ios)

  IF (ios /= 0) THEN
    print *,'no file ntm_deff_inputs.xml found either, should give input file'
    stop
  end IF

  PRINT *,' inputs will be taken from file ntm_deff_inputs.xml'
  n_lines = 0
  DO
    READ (INXML, '(a)', iostat = ios) xml_line
    if (ios == 0) then
      n_lines = n_lines + 1
    else
      exit
    end if
  END DO
  allocate(parameters(n_lines))
  rewind INXML
  do i = 1, n_lines
    read (INXML, '(a)', iostat = ios) parameters(i)
  end do
  close(INXML)
  !-- copy to codeparam structure
  allocate(codeparam_param%parameters_value(n_lines))
  codeparam_param%parameters_value = parameters
  allocate(codeparam_param%parameters_default(n_lines))
  codeparam_param%parameters_default = parameters
  !
  ! need also schemas in codeparam_param%schema
  !
  ! file_xml_schema = 'ntm_deff_schemas.xml'
  OPEN (unit = INXML, file = "ntm_deff_schemas.xml", status = 'old', &
    action = 'read', iostat = ios)
  n_lines = 0
  DO
    READ (INXML, '(a)', iostat = ios) xml_line
    if (ios == 0) then
      n_lines = n_lines + 1
    else
      exit
    end if
  END DO
  rewind INXML
  deallocate(parameters)
  allocate(parameters(n_lines))
  do i = 1, n_lines
    read (INXML, '(a)', iostat = ios) parameters(i)
  end do
  close(INXML)
  ! PRINT *,'parameters'
  ! PRINT *,parameters(:)
  allocate(codeparam_param%schema(n_lines))
  codeparam_param%schema = parameters
  !
  !  write(*,*) 'Open shot in MDS !'
  !  write(20,*) 'Open shot in MDS !'
  !  call ids_open(treename,shot,runin,idxin)
  !   print *, 'idxin',idxin

  ! call ids_get(idxin,"coretransp",coretransp_in)


  ! shot = 11
  !shot = 12
  ! shot = 10
  ! shot = 5 
  shot = 10
  !  runin = 4
  runin = 55
  !  runin = 2  
  !  runin = 66 
  !!runin = 31  
  ! runout = 32
  ! runout = 4  
  runout = 55 

  ! shot=77922
  shot=90279
  shot=92436
  runin = 1101
  ! runout = 999
  runout = 110
  runin = 2761
  runout = 9999

  shot = 65565
  runin = 102
  runout = 999
  treename = 'ids' ! Mandatory, do not change
  tree_user = 'g2snowak'
  tree_user = 'g2osaute'
  tree_user = 'g2tjohns'
  tree_tokamak = 'jet'
  tree_tokamak = 'test'
  tree_tokamak = 'tcv'

  call imas_open_env(treename,shot,runin,idxin,trim(tree_user),trim(tree_tokamak),'3')
  print *, 'idxin',idxin
!!$  !  runh  = 200
!!$  runh  = 300
!!$  runh = 1101
!!$  call ids_open(treename,shot,runh ,idxmhd)  

  call ids_get(idxin,"core_profiles",core_profiles_all)

  i_interpol = 1 ! closest time
  nb_transport_model = 1
  ! do it=1,size(core_profiles_all%time)
  do jt=1,1 

    call ids_get_slice(idxin,"core_profiles",core_profiles_slice,core_profiles_all%time(jt),i_interpol)
    call ids_get_slice(idxin,"core_transport",coretransp_in_slice,core_profiles_all%time(jt),i_interpol)
    call ids_get_slice(idxin,"ntms",ntms_in,core_profiles_all%time(jt),i_interpol)

    print *, 'before sub'
    call ntm_DeffTLets(core_profiles_slice, ntms_in, coretransp_in_slice, coretransp_out_slice,codeparam_param)
    print *, 'after sub'           
    print * ,   ' >>coretransp_out_slice%time= ',coretransp_out_slice%time
    print *, 'coretransp_out_slice%model(nb_transport_model)%profiles_1d(jt)%electrons%energy%d >>= ', & 
      & coretransp_out_slice%model(nb_transport_model)%profiles_1d(jt)%electrons%energy%d
    print *, 'coretransp_out_slice%model(nb_transport_model)%profiles_1d(jt)%electrons%particles%d >>= ', & 
      & coretransp_out_slice%model(nb_transport_model)%profiles_1d(jt)%electrons%particles%d

  end do

  write(*,*) 'Creating output run :'
  ! This is the UAL function that creates a shot in the ITM MDS+ tree
!!$  call ids_create(treename,shot,400   ,  0    ,  0   ,idxout)
!!$
!!$  print *, 'runout,refrun',runout,refrun
!!$  call ids_put(idxout,"coretransp",coretransp_out)

  write(*,*) 'Closing Database :'
  write(20,*) 'Closing Database :'
!!$  call ids_close(idxin,treename,shot,runin)
!!$  call ids_close(idxmhd,treename,shot,runh)
!!$  call ids_close(idxout,treename,shot,400)      

  write(*,*) 'Deallocate CPOs :'
  if (associated(coretransp_in%time))       call ids_deallocate(coretransp_in)  
  if (associated(coretransp_out%time))      call ids_deallocate(coretransp_out)  
  if (associated(ntms_in%time))              call ids_deallocate(ntms_in)  

end program ntm_DeffTL_progETS
