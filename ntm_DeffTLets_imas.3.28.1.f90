subroutine  ntm_DeffTLets( corep_in, ntms_in, coretransp_in, coretransp_out,params_in)

  use itm_types
  use coeffs_module

  use ids_schemas
  use ids_routines
  use interpos_module


  IMPLICIT NONE

  type (ids_core_transport) :: coretransp_in, coretransp_out
  type (ids_core_profiles)  :: corep_in
  type (ids_ntms)           :: ntms_in

  type (ids_Parameters_Input)  :: params_in       

  real(R8) :: mm, nn, qres, rhotors, width
  real(R8) :: wphase, wfreq, cepsilon, wdt
  real(R8), allocatable :: deffin(:), deffinc(:), Deffw(:), Deffwc(:), xx(:), xxc(:), espx(:), espxx(:)
  real(R8), allocatable :: deffvc(:)
  real(R8), allocatable ::  Deffwn(:), Deffwz(:), Deffwv(:)
  integer :: iunit, ntimes, it, nrho, nrhoc, last_iter, n, m, irhotors(1), istatus, ii, i
  integer :: iwi       
  integer :: kk        
  logical :: dbg = .false.

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  print *, 'in subroutine ntm_DeffTLets' 


  ! if dbg print *, 'in sub ntm_DeffTLets'

  cepsilon = 1.E-14_r8

  ! if dbg then
  if (.not. associated(params_in%parameters_value)) then
    write(0, *) 'ERROR: code parameters not associated!'
    return
  end if
  print *,params_in%parameters_value
  ! endif
  ! read values from xml file into variables
  call assign_code_parameters(params_in,istatus)
  ! if dbg then
  if (istatus /= 0) then
    write(*, *) 'ERROR: Could not assign some code parameters.'
    return
  end if

  ! print *, '*coretransp_out%time',coretransp_out%time
  ! if dbg print *, '*coretransp_in%time',coretransp_in%time

  ! if dbg print *,'coretransp_in%te_transp%diff_eff= ',coretransp_in%values(1)%te_transp%diff_eff
  print *,'coeff_deff_te= ',coeff_deff_te
  print *,'coeff_width_te= ',coeff_width_te
  print *,'coeff_deff_ti= ',coeff_deff_ti
  print *,'coeff_deff_ni= ',coeff_deff_ni   
  write(42,*) 'coeff_deff_te= ',coeff_deff_te
  write(42,*) 'coeff_deff_ne= ',coeff_deff_ne
  write(42,*) 'coeff_width_te= ',coeff_width_te
  write(42,*) 'coeff_width_ne= ',coeff_width_ne
  write(42,*) 'coeff_deff_ti= ',coeff_deff_ti
  write(42,*) 'coeff_deff_ni= ',coeff_deff_ni   

  ! endif

  ntimes=size(coretransp_in%time)
  ! allocate(coretransp_out(1))
  ! allocate(coretransp_out(2))
  ! allocate(coretransp_out(ntimes))
  print *,'ntimes= ',ntimes
  write(42,*) 'ntimes= ',ntimes


  !if (.not.associated(coretransp_out)) allocate(coretransp_out(1))
  ! jorge> to be sure that we start with an allocated coretransp out 
  ! with 1 element only (timeslice)
  ! jorge> copy all from coretransp_in to coretransp_out 
  call ids_copy(coretransp_in, coretransp_out)

  !!call ids_copy(coretransp_in(1),coretransp_out(1))
  ! call ids_copy(coretransp_in,coretransp_out)


  coretransp_out%time = coretransp_in%time
  ! jorge> this would only copy pointers to memory/data and both will share the same  
  ! coretransp_out(1) = coretransp_in(1)
  print *, 'size(coretransp_out%time)',size(coretransp_out%time)
  print *, 'coretransp_in%time',coretransp_in%time
  !      print *, 'coretransp_out%time',coretransp_out%time
  write(42,*)  'coretransp_in%time',coretransp_in%time

  print *,'size(ntms_in%time)= ',size(ntms_in%time)
  print *, 'ntms_in%time',ntms_in%time

  print *,'= coretransp_in%model(1)%profiles_1d(1)%electrons%energy%d',coretransp_in%model(1)%profiles_1d(1)%electrons%energy%d
  print *,'= coretransp_in%model(1)%profiles_1d(1)%electrons%particles%d',coretransp_in%model(1)%profiles_1d(1)%electrons%particles%d
  !PH
  print *, '*coretransp_out%time',coretransp_out%time

  it=1
  print *,'it= ',it    
  nrhoc=size(coretransp_in%model(1)%profiles_1d(it)%grid_v%rho_tor)
  print *,'nrhoc= ',nrhoc
  allocate(deffinc(nrhoc))
  allocate(Deffwc(nrhoc))
  allocate(Deffwz(nrhoc))
  allocate(Deffvc(nrhoc))
  allocate(xxc(nrhoc))

  nrho=size(corep_in%profiles_1d(it)%grid%rho_tor)
  print *,'nrho= ',nrho
  allocate(deffin(nrho))
  allocate(Deffw(nrho))
  allocate(Deffwn(nrho))
  allocate(xx(nrho))
  allocate(espx(nrho))
  allocate(espxx(nrho))

  write(42,*) 'nrhoc',nrhoc
  write(42,*) 'nrho ',nrho 

  nn = ntms_in%time_slice(it)%mode(1)%n_tor
  mm = ntms_in%time_slice(it)%mode(1)%m_pol
  qres=real(mm,r8) / real(nn,r8)              

  print *, 'ntms_in%time(it)',ntms_in%time(it)

  last_iter=1
!!$  do iwi=1,500
!!$    if (associated(ntms_in(it)%n(1)%plasma%disp_par)) then
!!$      if (real(ntms_in(it)%n(1)%plasma%disp_par(iwi,1)) > 0) last_iter=iwi
!!$    else
!!$      last_iter = iwi
!!$    end if
!!$  enddo
  if (associated(ntms_in%time_slice(it)%mode)) then
    width = ntms_in%time_slice(it)%mode(1)%width
    rhotors = ntms_in%time_slice(it)%mode(1)%rho_tor
    wphase = ntms_in%time_slice(it)%mode(1)%phase
    wfreq = ntms_in%time_slice(it)%mode(1)%frequency
  else
    ! test values
    width = 0.02_r8
    rhotors = 0.7_r8
    wphase = 0._r8
    wfreq = 10.e3_r8
  end if
  print *, 'last_iter',last_iter
  print *, 'width mhd',width
  print *,'rhotors',rhotors
  write(42,*) 'last_iter',last_iter
  write(42,*)  'width mhd',width
  write(42,*) 'rhotors',rhotors
  write(20,*) 'rhotors',rhotors

  ! construct added Deff

  ! Te
  print *,'size(coretransp_in%time)= ',size(coretransp_in%time)
  deffin =  coretransp_in%model(1)%profiles_1d(it)%electrons%energy%d
  print *, 'deffin',  deffin
  write(42,*) 'deffin',  deffin
  !  rhotors=rhotors/100.
  xx = 2.0*abs(corep_in%profiles_1d(it)%grid%rho_tor - rhotors)
  print *, '----->xx',xx  
  ! irhotors(1)=minloc(corep_in(it)%rho_tor,rhotors)
  irhotors(1)=minloc(xx,1)                            
  print *,'rhotors, width, qres, irhotors= ',rhotors, width, qres, irhotors
  print *,'irhotors= ',irhotors
  print *, 'width Deffw',width
  write(42,*) 'rhotors, width, qres, irhotors= ',rhotors, width, qres, irhotors
  write(42,*) 'irhotors= ',irhotors
  write(42,*) 'width Deffw',width
  write(20,*)  'coeff_width_te',coeff_width_te
  write(20,*)  'width Deffw',width
  !       coeff_width_te = 2.0
  !       coeff_deff_te = 5.0
  Deffw=coeff_deff_te * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  ! Deffw=                deffin(irhotors(1)) * exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *, 'espd', (coeff_width_te*width)**2
  espx =  exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *, 'espe', exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *,'Deffin',deffin(irhotors(1))
  print *, 'deffin*exp',deffin(irhotors(1)) * exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *,'Deffw_Te= ',Deffw
  !    allocate(coretransp_out(it)%te_transp%diff_eff(nrho))
  if(width .le. 0.0) wdt=0.0
  if(width .gt. 0.0) wdt=1.0
  ! coretransp_out(it)%values(1)%te_transp%diff_eff = deffin + Deffw * 1.0  
  coretransp_out%model(1)%profiles_1d(it)%electrons%particles%d = deffin + Deffw * wdt  
  ! coretransp_out(it)%te_transp%diff_eff = deffin 


  !print *, 'deffin+Deffw', deffin, Deffw, deffin+Deffw                             
  goto 2000
  ! ne
  ! print *,'coretransp_in(it)%ne_transp%diff_eff(1,:)',coretransp_in(it)%ne_transp%diff_eff(1,:)
  ! deffin =  coretransp_in(it)%values(1)%ne_transp%diff_eff(:,1)
  deffin =  coretransp_in%model(1)%profiles_1d(it)%electrons%particles%d
  write(42,*) 'ne  deffin', deffin
  do i=1,size(coretransp_in%model)
    write(*,*)  'in',i,'_coretransp_ne',coretransp_in%model(i)%profiles_1d(it)%electrons%particles%d
    write(42,*)  'in',i,'_coretransp_ne',coretransp_in%model(i)%profiles_1d(it)%electrons%particles%d
    write(42,*)
  end do

  write(42,*)  'coeff_deff_ne ',coeff_deff_ne    
  write(42,*)  'coeff_width_ne',coeff_width_ne
  write(*,*)  'coeff_deff_ne ',coeff_deff_ne    
  write(*,*)  'coeff_width_ne',coeff_width_ne
  Deffwn=coeff_deff_ne * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
  !print *,'Deffw= ',Deffw
  espxx=  exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
  !      allocate(coretransp_out(it)%ne_transp%diff_eff(nrho,3))
  ! coretransp_out(it)%values(1)%ne_transp%diff_eff(:,1) = deffin + Deffw
  ! coretransp_out(it)%values(1)%ne_transp%diff_eff(:,1) = deffin + Deffwn*wdt
  coretransp_out%model(1)%profiles_1d(it)%electrons%particles%d = deffin + Deffwn          
  write(42,*)  'Out1_coretransp_ne',coretransp_out%model(1)%profiles_1d(it)%electrons%particles%d

  goto 2000

  ! Ti
  deffinc= coretransp_in%model(1)%profiles_1d(it)%total_ion_energy%d
  print *,'coretransp_in%model(1)%profiles_1d(it)%total_ion_energy%d= ',deffinc
  Deffwc=coeff_deff_ti * deffinc(irhotors(1)) * exp(-xxc**2/(coeff_width_te*width+cepsilon)**2)
  !      allocate(coretransp_out(it)%ti_transp%diff_eff(nrho,1))
  !   deffin = coretransp_in(it)%ti_transp%diff_eff(:,1)
  ! coretransp_out(it)%values(1)%ti_transp%diff_eff(:,1) = deffinc + Deffwc
  coretransp_out%model(1)%profiles_1d(it)%total_ion_energy%d = deffinc + Deffwc *wdt
  !      print *,'coretransp_out(it)%ti_transp%diff_eff(:,1)= ',coretransp_out(it)%values(1)%ti_transp%diff_eff(:,1)

  ! ni
  deffinc= coretransp_in%model(1)%profiles_1d(it)%ion(1)%particles%d
  print *,'coretransp_in%model(1)%profiles_1d(it)%ion(1)%particles%d = ',deffinc
  xxc=2.0*abs(coretransp_in%model(1)%profiles_1d(it)%grid_v%rho_tor-rhotors)
  print *, 'XXc_ni ', xxc
  !xx=abs(coretransp_in(it)%rho_tor(:)-rhotors)
  irhotors(1)=minloc(xxc,1)
  write(*,*) '==> irhotors(1)), rhotors',irhotors(1),rhotors
  Deffwc=coeff_deff_ni * deffin(irhotors(1)) * exp(-xxc**2/(coeff_width_ne*width+cepsilon)**2)
  !    allocate(coretransp_out(it)%ni_transp%diff_eff(nrho,1,3))
  ! coretransp_out(it)%values(1)%ni_transp%diff_eff(:,1,3) = deffinc + Deffwc
  ! coretransp_out(it)%values(1)%ni_transp%diff_eff(:,1,3) = deffinc + Deffwc * wdt
  coretransp_out%model(1)%profiles_1d(it)%ion(1)%particles%d = deffinc + Deffwc               
  !      print *,'coretransp_out(it)%ni_transp%diff_eff(:,1,3)= ',coretransp_out(it)%values(1)%ni_transp%diff_eff(:,1,3)
  print *, 'end Ti ni'



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! nz
!!$    write(42,*) 'coeff_deff_ni, coeff_width_ne,width'
!!$    write(42,*)  coeff_deff_ni, coeff_width_ne,width 
!!$  print *,'size(coretransp_in)= ',size(coretransp_in)
!!$  if (.not. associated(coretransp_in(it)%values(1)%nz_transp(1)%diff_eff) .or. size(coretransp_in).eq.0 ) then
!!$    print *,'coretransp_in(it)%nz_transp%diff_eff not associated'
!!$    return
!!$  end if
!!$     print *, 'size rhotor', size(corep_in(it)%rho_tor)
!!$! print *,'it, corep_in(it)%rho_tor',it, corep_in(it)%rho_tor              
!!$! print *,'it, coretransp_in(it)%nz_transp%diff_eff',it, coretransp_in(it)%values(1)%te_transp%diff_eff
!!$! print *,'size te_transp', size(coretransp_in(it)%values(1)%te_transp%diff_eff)
!!$! deffin =  coretransp_in(it)%values(1)%nz_transp(1)%diff_eff(:,74)
!!$
!!$    do kk=1,74  
!!$
!!$    do ii=1,100
!!$! deffin(ii) =  coretransp_in(it)%values(1)%nz_transp(1)%vconv_eff(ii,kk)
!!$  deffin(ii) =  coretransp_in(it)%values(1)%nz_transp(1)%diff_eff(ii,kk)
!!$  deffvc(ii) =  coretransp_in(it)%values(1)%nz_transp(1)%vconv_eff(ii,kk)
!!$    enddo
!!$    do ii=1,100
!!$    if(deffin(ii) .le. 0.0) deffin(ii)=1.0e-05
!!$    if(deffvc(ii) .le. 0.0) deffvc(ii)=1.0e-05
!!$    enddo
!!$! print *, 'deffin',  coretransp_in(it)%values(1)%te_transp%diff_eff
!!$! write(42,*) 'nz_deffin',  coretransp_in(it)%values(1)%nz_transp(1)%vconv_eff(:,45)
!!$  write(44,*) 'it',it
!!$  write(44,*) 'nz_deffin',coretransp_in(it)%values(1)%rho_tor,coretransp_in(it)%values(1)%nz_transp(1)%diff_eff(:,45)
!!$  write(44,*) 'nz_deffvc',coretransp_in(it)%values(1)%rho_tor,coretransp_in(it)%values(1)%nz_transp(1)%vconv_eff(:,45)
!!$  write(44,*)
!!$!  rhotors=rhotors/100.
!!$  print *,'core%rho',corep_in(it)%rho_tor(:)
!!$  print *, 'rhotors',rhotors
!!$   write(42,*) 'rhotors', rhotors
!!$  xx=2.0*abs(corep_in(it)%rho_tor-rhotors)
!!$  print *, '----->xx',xx  
!!$! irhotors(1)=minloc(corep_in(it)%rho_tor,rhotors)
!!$  irhotors(1)=minloc(xx,1)                            
!!$  print *,'rhotors, width, qres, irhotors= ',rhotors, width, qres, irhotors
!!$  print *,'irhotors= ',irhotors
!!$  print *, 'width Deffw',width
!!$  write(42,*) 'rhotors, width, qres, irhotors= ',rhotors, width, qres, irhotors
!!$  write(42,*) 'irhotors= ',irhotors
!!$  write(42,*) 'width Deffw',width
!!$  write(20,*)  'coeff_width_nz',coeff_width_ne
!!$  write(20,*)  'width Deffw',width
!!$!       coeff_width_te = 2.0
!!$!       coeff_deff_te = 5.0
!!$    write(42,*) 'coeff_deff_ni',coeff_deff_ni
!!$    write(42,*) 'coeff_width_ne',coeff_width_ne    
!!$  Deffwz=     coeff_deff_ni * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!!$! Deffwz=100.*coeff_deff_ni * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!!$! Deffwz=100.*5000000.      * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!!$  Deffwv=-coeff_deff_other * deffvc(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!!$! Deffwv=-10000.*coeff_deff_ni * deffvc(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!!$! Deffwv=-1000.*50000000.         * deffvc(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!!$! Deffw=                deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!!$  print *, 'espd', (coeff_width_te*width)**2
!!$  espxx=  exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!!$  print *, 'espe', exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!!$  print *,'Deffin',deffin(irhotors(1))
!!$  print *, 'deffin*exp',deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!!$  print *,'Deffw_nz= ',Deffwz
!!$  !    allocate(coretransp_out(it)%te_transp%diff_eff(nrho))
!!$  if(width .le. 0.0) wdt=0.0
!!$  if(width .gt. 0.0) wdt=1.0
!!$! coretransp_out(it)%values(1)%nz_transp(1)%diff_eff(:,74) = deffin + Deffw * wdt  
!!$! coretransp_out(it)%values(1)%nz_transp(1)%vconv_eff(:,74) = deffin + Deffw * wdt  
!!$    write(42,*) 'WIDTH, WDT', width, wdt
!!$   do ii=1,100
!!$! coretransp_out(it)%values(1)%nz_transp(1)%vconv_eff(ii,kk) = deffin(ii) + Deffwz(ii)* wdt  
!!$  coretransp_out(it)%values(1)%nz_transp(1)%diff_eff(ii,kk) = deffin(ii) + Deffwz(ii)* wdt  
!!$  coretransp_out(it)%values(1)%nz_transp(1)%vconv_eff(ii,kk) = deffvc(ii) + Deffwv(ii)* wdt  
!!$  write(42,*) 'do_nz', ii,kk,deffin(ii),Deffwz(ii),width, wdt,  &  
!!$  coretransp_out(it)%values(1)%nz_transp(1)%diff_eff(ii,kk),  &
!!$  coretransp_out(it)%values(1)%nz_transp(1)%vconv_eff(ii,kk) 
!!$    enddo
!!$
!!$    enddo
!!$

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !  2000   continue

  do ii=1,nrho                  

    if(Deffw(ii)  <= 1.0e-99) Deffw(ii)=1.e-99
    if(espx(ii)  <= 1.0e-99) espx(ii)=1.e-99
    if(espxx(ii)  <= 1.0e-99) espxx(ii)=1.e-99
    !  if(Deffwc(ii)  <= 1.0e-99) Deffwc(ii)=1.e-99
    !  write(*,*) 'irhotors(1))',irhotors(1)
    !CAD2014 write(42,42) ii,corep_in(it)%rho_tor(ii),espx(ii),Deffw(ii),coretransp_in(it)%values(1)%te_transp%diff_eff(ii), & 
    !   coretransp_out(it)%values(1)%te_transp%diff_eff(ii),corep_in(it)%te%value(ii),width,   &
    !   corep_in(it)%time,ntms_in(it)%time,coretransp_in(it)%time,coretransp_out(it)%time,              &
    !       deffin(irhotors(1)), coeff_width_te, coeff_deff_te
    !   coretransp_out(it)%ne_transp%diff_eff(ii,1),coretransp_out(it)%ti_transp%diff_eff(ii,1)
    !   coretransp_out(it)%ni_transp%diff_eff(ii,1,1)
  enddo

2000 continue         

  print *, 'end of Subroutine it',it

  ! end do
42 format(i5,16(1pe12.4))

  rewind 42
  !rewind 88

  deallocate(deffinc)
  deallocate(Deffwc)
  deallocate(xxc)

  deallocate(deffin)
  deallocate(Deffw)
  deallocate(xx)
  deallocate(espx)
  deallocate(espxx)

  print *, 'end of ntm_DeffTLets routine'


end subroutine ntm_DeffTLets
