subroutine  ntm_DeffTLets( corep_in, mhd_in, coretransp_in, coretransp_out,params_in)

  use itm_types
  use coeffs_module

  use euITM_schemas
  use euITM_routines
  USE interpos_module


  IMPLICIT NONE

  type (type_coretransp), pointer ::     coretransp_in(:), coretransp_out(:)
  type (type_coreprof), pointer ::         corep_in(:)
  type (type_mhd), pointer ::          mhd_in(:)

  type (type_param)  :: params_in       

  real(R8) :: mm, nn, qres, rhotors, width
  real(R8) :: wphase, wfreq, cepsilon, wdt
  real(R8), allocatable :: deffin(:), deffinc(:), Deffw(:), Deffwc(:), xx(:), xxc(:), espx(:), espxx(:)
  integer :: iunit, ntimes, it, nrho, nrhoc, last_iter, n, m, irhotors(1), istatus, ii
  integer :: itt, ici
  integer :: iwi       

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  print *, 'in sub ntm_DeffTLets'
  cepsilon = 1.E-14_r8
  if (.not. associated(params_in%parameters)) then
    write(0, *) 'ERROR: code parameters not associated!'
    return
  end if
  print *,params_in%parameters
  ! read values from xml file into variables
  call assign_code_parameters(params_in,istatus)
  if (istatus /= 0) then
    write(*, *) 'ERROR: Could not assign some code parameters.'
    return
  end if

! print *, '*coretransp_out%time',coretransp_out(1)%time
  print *, '*coretransp_in%time',coretransp_in(1)%time

  print *,'coretransp_in%te_transp%diff_eff= ',coretransp_in(1)%values(1)%te_transp%diff_eff
  print *,'coeff_deff_te= ',coeff_deff_te
  print *,'coeff_width_te= ',coeff_width_te
  print *,'coeff_deff_ti= ',coeff_deff_ti
  print *,'coeff_deff_ni= ',coeff_deff_ni   

  ntimes=size(coretransp_in)
  ! allocate(coretransp_out(1))
  ! allocate(coretransp_out(2))
  ! allocate(coretransp_out(ntimes))
  print *,'ntimes= ',ntimes

! call euITM_copy(coretransp_in(1),coretransp_out(1))
  call euITM_copy(coretransp_in,coretransp_out)
!     coretransp_out(1) = coretransp_in(1)
       print *, 'size(coretransp_out)',size(coretransp_out)
       print *, 'coretransp_in(1)%time',coretransp_in(1)%time
       print *, 'coretransp_out(1)%time',coretransp_out(1)%time

  print *,'size(mhd_in)= ',size(mhd_in)
  ! it=1
  print *, 'mhd_in%time',mhd_in%time
 !print *,'coretransp_in%te_transp%diff_eff= ',coretransp_in(1)%te_transp%diff_eff

  ! do itt=1,size(mhd_in)
  ! do itt=1,ntimes         
  ! do itt=1,2              
  !    nrho=size(coretransp_in(itt)%rho_tor)
 !nrho=size(coretransp_in(ntimes)%rho_tor)
  nrho=size(corep_in(ntimes)%rho_tor)
  itt = 1      
  print *, 'itt,nrho',itt,nrho
  print *,'coretransp_in%te_transp%diff_eff= ',coretransp_in(1)%values(1)%te_transp%diff_eff
  !PH
  !allocate(coretransp_out(itt))
  !coretransp_out(1)%time=coretransp_in(1)%time
  ! coretransp_out(2)%time=coretransp_in(2)%time
  ! print *, '*coretransp_out%time',coretransp_out(1)%time
 print *, '*coretransp_out%time',coretransp_out(1)%time
  do ici=1,itt
                 if (.not.associated(coretransp_out(ici)%values(1)%te_transp%diff_eff)) & 
                    allocate(coretransp_out(ici)%values(1)%te_transp%diff_eff(nrho))

                 if (.not.associated(coretransp_out(ici)%values(1)%ne_transp%diff_eff)) & 
                    allocate(coretransp_out(ici)%values(1)%ne_transp%diff_eff(nrho,1))

                 if (.not.associated(coretransp_out(ici)%values(1)%ti_transp%diff_eff)) &
                    allocate(coretransp_out(ici)%values(1)%ti_transp%diff_eff(nrho,1))

                 if (.not.associated(coretransp_out(ici)%values(1)%ni_transp%diff_eff)) &
                   allocate(coretransp_out(ici)%values(1)%ni_transp%diff_eff(nrho,1,1))

  end do
  print *,'coretransp_in(1)%te_transp%diff_eff= ',coretransp_in(1)%values(1)%te_transp%diff_eff

! print *, '*coretransp_out%time',coretransp_out(1)%time
  print *,'coretransp_in(1)%te_transp%diff_eff= ',coretransp_in(1)%values(1)%te_transp%diff_eff
  !do ici=1,nrho
  !  coretransp_out(itt)%te_transp%diff_eff(ici)=coretransp_in(itt)%te_transp%diff_eff(ici)
  !  coretransp_out(itt)%ne_transp%diff_eff(ici,1)=coretransp_in(itt)%te_transp%diff_eff(ici)
  !  coretransp_out(itt)%ti_transp%diff_eff(ici,1)=coretransp_in(itt)%te_transp%diff_eff(ici)
  !  coretransp_out(itt)%ni_transp%diff_eff(ici,1,1)=coretransp_in(itt)%te_transp%diff_eff(ici)
  !enddo

  print *,'>itt, coretransp_out(it)%te_transp%diff_eff',itt, coretransp_out(itt)%values(1)%te_transp%diff_eff
  print *,'>itt, coretransp_out(it)%ne_transp%diff_eff',itt, coretransp_out(itt)%values(1)%ne_transp%diff_eff(:,1)
! print *,'size(mhd_in(itt)%disp_par,1)= ',size(mhd_in(itt)%plasma%disp_par,1)
! print *,'size(mhd_in(itt)%disp_par,2)= ',size(mhd_in(itt)%plasma%disp_par,2)
! print *,'size(mhd_in(itt)%disp_par,3)= ',size(mhd_in(itt)%plasma%disp_par,3)
! print *,'size(mhd_in(itt)%n)= ',size(mhd_in(itt)%n)
! print *, mhd_in(itt)%n(1)
! print *, mhd_in(itt)%plasma%m(1,1,1)
! print *, mhd_in(itt)%frequency(1)
! print *, 'width',mhd_in(itt)%plasma%disp_par(size(mhd_in(itt)%plasma%disp_par,1),1,1)
  ! end do

  ! do it=1,1
  !    it=itt-1
  it=itt
  print *,'it= ',it    
  nrhoc=size(coretransp_in(it)%values(1)%rho_tor)
  print *,'nrhoc= ',nrhoc
  allocate(deffinc(nrhoc))
  allocate(Deffwc(nrhoc))
  allocate(xxc(nrhoc))

  nrho=size(corep_in(it)%rho_tor)
  print *,'nrho= ',nrho
  allocate(deffin(nrho))
  allocate(Deffw(nrho))
  allocate(xx(nrho))
  allocate(espx(nrho))
  allocate(espxx(nrho))

  !    coretransp_out(it)%time=real(it,R8)
! print *,' coretransp_out(it)%time= ',coretransp_out(it)%time
! last_iter=size(mhd_in(it)%plasma%disp_par,1)
! print *,'last_iter= ',last_iter
! n=mhd_in(it)%n(1)
! m=mhd_in(it)%plasma%m(1,n,1)
!  n=1
!  m=2

   mm = mhd_in(it)%plasma%m(1,1,1) 
   nn = mhd_in(it)%n(1) 
  print *,'mhd_in(it)%n(1), mhd_in(it)%plasma%m(1,1,1)'
  print *, mhd_in(it)%n(1), mhd_in(it)%plasma%m(1,1,1) 
!  rewind 88
!  write(88,*) mhd_in(it)%plasma%m(1,1,1), mhd_in(it)%n
!  rewind 88
!  read(88,*) mm, nn
!  write(88,*) mm, nn

! qres=real(mm,R8) / real(nn,R8)
! qres=mm / mhd_in(it)%n
  qres=mm / nn              
!j write(88,*) 'in ntmDeff n,m', mhd_in(it)%n(1), mhd_in(it)%plasma%m(1,1,1)
!j write(88,*) 'in ntmDeff n,m,qres', mm, nn, qres                                 
  !    rhotors=mhd_in(1)%disp_par(last_iter,n,7)
  !    width=mhd_in(1)%disp_par(last_iter,n,1)
  
  print *, 'mhd_in%time',mhd_in(it)%time
! print *, 'par20,1,7',mhd_in(1)%plasma%disp_par%re(20,1,7)

! rhotors=mhd_in(it)%plasma%disp_par(1,n,7)
!!rhotors=mhd_in(it)%plasma%disp_par(20,1,7)
!   rhotors=0.29
!   rhotors=0.8 
! print *,'*rhotors1',rhotors
! rhotors=mhd_in(it)%disp_par(last_iter,n,7)
! print *,'*rhotors2',rhotors
! width=mhd_in(it)%plasma%disp_par(last_iter,n,1)

  last_iter=1
! do iwi=2,500
  do iwi=1,500
!    if (mhd_in(it)%plasma%disp_par%re(iwi,n,1) > 0) last_iter=iwi
     if (mhd_in(it)%plasma%disp_par%re(iwi,1,1) > 0) last_iter=iwi
!    print *, '***',  iwi, last_iter, mhd_in(it)%plasma%disp_par%re(iwi,n,1) 
!    print *, '***',  iwi, last_iter, mhd_in(it)%plasma%disp_par%re(iwi,1,1) 
  enddo
! width=mhd_in(it)%plasma%disp_par(20,       n,1)
! width=mhd_in(it)%plasma%disp_par%re(last_iter,n,1)
! rhotors=mhd_in(it)%plasma%disp_par%re(last_iter,n,7)
  width=mhd_in(it)%plasma%disp_par%re(last_iter,1,1)
  rhotors=mhd_in(it)%plasma%disp_par%re(last_iter,1,7)
  print *, 'last_iter',last_iter
  print *, 'width mhd',width
  print *,'rhotors',rhotors
  write(20,*) 'rhotors',rhotors
!  wphase=mhd_in(1)%plasma%disp_par%re(last_iter,n,2)
!  wfreq=mhd_in(1)%plasma%disp_par%re(last_iter,n,3)
   wphase=mhd_in(1)%plasma%disp_par%re(last_iter,1,2)
   wfreq=mhd_in(1)%plasma%disp_par%re(last_iter,1,3)

  ! construct added Deff

  ! Te
  print *,'size(coretransp_in)= ',size(coretransp_in)
  if (.not. associated(coretransp_in(it)%values(1)%te_transp%diff_eff) .or. size(coretransp_in).eq.0 ) then
    print *,'coretransp_in(it)%te_transp%diff_eff not associated'
    return
  end if
     print *, 'size rhotor', size(corep_in(it)%rho_tor)
  print *,'it, corep_in(it)%rho_tor',it, corep_in(it)%rho_tor              
  print *,'it, coretransp_in(it)%te_transp%diff_eff',it, coretransp_in(it)%values(1)%te_transp%diff_eff
  print *,'size te_transp', size(coretransp_in(it)%values(1)%te_transp%diff_eff)
  deffin =  coretransp_in(it)%values(1)%te_transp%diff_eff
  print *, 'deffin',  coretransp_in(it)%values(1)%te_transp%diff_eff
  write(42,*) 'deffin',  coretransp_in(it)%values(1)%te_transp%diff_eff
!  rhotors=rhotors/100.
  print *,'core%rho',corep_in(it)%rho_tor(:)
  print *, 'rhotors',rhotors
  xx=2.0*abs(corep_in(it)%rho_tor-rhotors)
  print *, '----->xx',xx  
! irhotors(1)=minloc(corep_in(it)%rho_tor,rhotors)
  irhotors(1)=minloc(xx,1)                            
  print *,'rhotors, width, qres, irhotors= ',rhotors, width, qres, irhotors
  print *,'irhotors= ',irhotors
  print *, 'width Deffw',width
  write(20,*)  'coeff_width_te',coeff_width_te
  write(20,*)  'width Deffw',width
!       coeff_width_te = 2.0
!       coeff_deff_te = 5.0
  Deffw=coeff_deff_te * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
! Deffw=                deffin(irhotors(1)) * exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *, 'espd', (coeff_width_te*width)**2
  espx =  exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *, 'espe', exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *,'Deffin',deffin(irhotors(1))
  print *, 'deffin*exp',deffin(irhotors(1)) * exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *,'Deffw_Te= ',Deffw
  !    allocate(coretransp_out(it)%te_transp%diff_eff(nrho))
  if(width .le. 0.0) wdt=0.0
  if(width .gt. 0.0) wdt=1.0
! coretransp_out(it)%values(1)%te_transp%diff_eff = deffin + Deffw * 1.0  
  coretransp_out(it)%values(1)%te_transp%diff_eff = deffin + Deffw * wdt  
! coretransp_out(it)%te_transp%diff_eff = deffin 


 !print *, 'deffin+Deffw', deffin, Deffw, deffin+Deffw                             

  ! ne
  ! print *,'coretransp_in(it)%ne_transp%diff_eff(1,:)',coretransp_in(it)%ne_transp%diff_eff(1,:)
    deffin =  coretransp_in(it)%values(1)%ne_transp%diff_eff(:,1)
  
 !print *,'coretransp_in(it)%rho_tor(:)= ',coretransp_in(it)%rho_tor(:)
 !print *,'rhotors= ',rhotors
! xx=abs(coretransp_in(it)%rho_tor(:)-rhotors)
  xx=2.0*abs(corep_in(it)%rho_tor-rhotors)
! irhotors(1)=minloc(xx,1)
  Deffw=coeff_deff_ne * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
 !print *,'Deffw= ',Deffw
  espxx=  exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!      allocate(coretransp_out(it)%ne_transp%diff_eff(nrho,3))
! coretransp_out(it)%values(1)%ne_transp%diff_eff(:,1) = deffin + Deffw
  coretransp_out(it)%values(1)%ne_transp%diff_eff(:,1) = deffin + Deffw *wdt


  ! Ti
       print *,'coretransp_in(it)%ti_transp%diff_eff(1,:)= ',coretransp_in(it)%values(1)%ti_transp%diff_eff(1,:)
       deffinc= coretransp_in(it)%values(1)%ti_transp%diff_eff(:,1)
  print *,'deffin Ti= ',deffinc
  xxc=2.0*abs(coretransp_in(it)%values(1)%rho_tor(:)-rhotors)
! xx=2.0*abs(corep_in(it)%rho_tor-rhotors)
  print *, 'XXc_Ti ', xxc
  irhotors(1)=minloc(xxc,1)                            
   write(*,*) '==>> irhotors(1)), rhotors',irhotors(1),rhotors
!   deffin = coretransp_in(it)%values(1)%ti_transp%diff_eff(:,1)
  Deffwc=coeff_deff_ti * deffinc(irhotors(1)) * exp(-xxc**2/(coeff_width_te*width+cepsilon)**2)
!      allocate(coretransp_out(it)%ti_transp%diff_eff(nrho,1))
!   deffin = coretransp_in(it)%ti_transp%diff_eff(:,1)
! coretransp_out(it)%values(1)%ti_transp%diff_eff(:,1) = deffinc + Deffwc
  coretransp_out(it)%values(1)%ti_transp%diff_eff(:,1) = deffinc + Deffwc *wdt
       print *,'coretransp_out(it)%ti_transp%diff_eff(:,1)= ',coretransp_out(it)%values(1)%ti_transp%diff_eff(:,1)

  ! ni
!      print *,'coretransp_in(it)%ni_transp%diff_eff(:,1,1)',coretransp_in(it)%values(1)%ni_transp%diff_eff(:,1,1)
!      print *, 'ni_transp'
!      deffinc= coretransp_in(it)%values(1)%ni_transp%diff_eff(:,1,1)
!      print *, 'Deffinc_ni',deffinc
! xxc=2.0*abs(coretransp_in(it)%values(1)%rho_tor(:)-rhotors)
! print *, 'XXc_ni ', xxc
 !xx=abs(coretransp_in(it)%rho_tor(:)-rhotors)
! irhotors(1)=minloc(xxc,1)
!  write(*,*) '==> irhotors(1)), rhotors',irhotors(1),rhotors
!  Deffwc=coeff_deff_ni * deffin(irhotors(1)) * exp(-xxc**2/(coeff_width_ne*width+cepsilon)**2)
  !    allocate(coretransp_out(it)%ni_transp%diff_eff(nrho,1,3))
! coretransp_out(it)%values(1)%ni_transp%diff_eff(:,1,3) = deffinc + Deffwc
! coretransp_out(it)%values(1)%ni_transp%diff_eff(:,1,3) = deffinc + Deffwc * wdt
!      print *,'coretransp_out(it)%ni_transp%diff_eff(:,1,3)= ',coretransp_out(it)%values(1)%ni_transp%diff_eff(:,1,3)
!      print *, 'end Ti ni'

  do ii=1,nrho                  
 !  write(42,42) ii,coretransp_in(it)%rho_tor(ii),espx(ii),coretransp_in(it)%te_transp%diff_eff(ii), & 
    write(42,42) ii,corep_in(it)%rho_tor(ii),coretransp_in(it)%values(1)%te_transp%diff_eff(ii), & 
     coretransp_out(it)%values(1)%te_transp%diff_eff(ii)
   if(espx(ii)  <= 1.0e-99) espx(ii)=1.e-99
   if(Deffw(ii)  <= 1.0e-99) Deffw(ii)=1.e-99
!  write(*,*) 'irhotors(1))',irhotors(1)
 !CAD2014 write(42,42) ii,corep_in(it)%rho_tor(ii),espx(ii),Deffw(ii),coretransp_in(it)%values(1)%te_transp%diff_eff(ii), & 
 !   coretransp_out(it)%values(1)%te_transp%diff_eff(ii),corep_in(it)%te%value(ii),width,   &
 !   corep_in(it)%time,mhd_in(it)%time,coretransp_in(it)%time,coretransp_out(it)%time,              &
 !       deffin(irhotors(1)), coeff_width_te, coeff_deff_te
 !   coretransp_out(it)%ne_transp%diff_eff(ii,1),coretransp_out(it)%ti_transp%diff_eff(ii,1)
 !   coretransp_out(it)%ni_transp%diff_eff(ii,1,1)
  enddo
  print *, 'end of Subroutine it',it

  ! end do
 42   format(i5,16(1pe12.4))

    rewind 42
    rewind 88

end subroutine ntm_DeffTLets
