MODULE  coeffs_module
   
  USE itm_types

  implicit none

  REAL(R8) :: coeff_deff_te, coeff_deff_ne, coeff_deff_ti, coeff_deff_ni, coeff_deff_other, coeff_width_te, coeff_width_ne

END MODULE coeffs_module
