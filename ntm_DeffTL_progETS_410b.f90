program ntm_DeffTL_progETS

  use itm_types     
  use euITM_schemas
  use euITM_routines

  implicit none

  interface

      subroutine  ntm_DeffTLets( cor_in,mhd_in, coretransp_in, coretransp_out,param_in)
       ! subroutine  ntm_Deff( mhd_in, coretransp_in, coretransp_out, param_in)

       use euITM_schemas
       USE interpos_module

       USE itm_types

       type (type_coretransp), pointer ::     coretransp_in(:), coretransp_out(:)
       type (type_coreprof), pointer ::        cor_in(:)
       type (type_mhd), pointer ::          mhd_in(:)

       type (type_param)  :: param_in         

     end subroutine ntm_DeffTLets
  end interface

  type (type_coretransp), pointer ::     coretransp_in(:), coretransp_out(:)
  type (type_coretransp), pointer ::     coretransp_in_slice(:), coretransp_out_slice(:)
  type (type_coreprof), pointer ::          cor_in(:), cor_slice(:)
  type (type_mhd), pointer ::          mhd_in(:)
  type (type_mhd), pointer ::          mhd_cpo(:)

  type (type_param) :: codeparam_param

  integer :: idxin, idxout, shot, runin, runout, refshot, refrun, idxmhd, runh
  integer :: i, INXML, ios, n_lines, jt

  character(len=5)::treename
  character(len = 132), allocatable :: parameters(:)
  character(len = 132) :: xml_line

  ! shot = 11
  ! runin = 24
  !! runin = 41
  ! runout = 32
  ! refshot = 0 ! Dummy, not used
  refrun =0   ! Dummy, not used
  treename = 'euitm' ! Mandatory, do not change


  ! read xml input file
  !
  INXML=1
  OPEN (unit = INXML, file = "ntm_deff_inputs.xml", status = 'old', &
    action = 'read', iostat = ios)

  IF (ios /= 0) THEN
    print *,'no file ntm_deff_inputs.xml found either, should give input file'
    stop
  end IF

  PRINT *,' inputs will be taken from file ntm_deff_inputs.xml'
  n_lines = 0
  DO
    READ (INXML, '(a)', iostat = ios) xml_line
    if (ios == 0) then
      n_lines = n_lines + 1
    else
      exit
    end if
  END DO
  allocate(parameters(n_lines))
  rewind INXML
  do i = 1, n_lines
    read (INXML, '(a)', iostat = ios) parameters(i)
  end do
  close(INXML)
  !-- copy to codeparam structure
  allocate(codeparam_param%parameters(n_lines))
  codeparam_param%parameters = parameters
  allocate(codeparam_param%default_param(n_lines))
  codeparam_param%default_param = parameters
  !
  ! need also schemas in codeparam_param%schema
  !
  ! file_xml_schema = 'ntm_deff_schemas.xml'
  OPEN (unit = INXML, file = "ntm_deff_schemas.xml", status = 'old', &
    action = 'read', iostat = ios)
  n_lines = 0
  DO
    READ (INXML, '(a)', iostat = ios) xml_line
    if (ios == 0) then
      n_lines = n_lines + 1
    else
      exit
    end if
  END DO
  rewind INXML
  deallocate(parameters)
  allocate(parameters(n_lines))
  do i = 1, n_lines
    read (INXML, '(a)', iostat = ios) parameters(i)
  end do
  close(INXML)
    PRINT *,'parameters'
    PRINT *,parameters(:)
  allocate(codeparam_param%schema(n_lines))
  codeparam_param%schema = parameters
  !
  !  write(*,*) 'Open shot in MDS !'
  !  write(20,*) 'Open shot in MDS !'
  !  call euitm_open(treename,shot,runin,idxin)
  !   print *, 'idxin',idxin

  ! call euitm_get(idxin,"coretransp",coretransp_in)

  ! print *,'size(coretransp_in) ',size(coretransp_in)

  ! shot = 11
 !shot = 12
! shot = 10
! shot = 5 
  shot = 10
!  runin = 4
   runin = 55
!  runin = 2  
!  runin = 66 
  !!runin = 31  
! runout = 32
! runout = 4  
  runout = 55 

! shot=77922
! shot=90279
  shot=92436
! runin = 1101
! runout = 999
! runout = 110
  runin = 2761
  runout = 9999

  call euitm_open(treename,shot,runin,idxin)    
! idxin=idxinmhd
! call euitm_get(idxin,"coreprof/1",cor_in)
! call euitm_get(idxin,"coreprof/2",cor_in)
!*call euitm_get(idxin,"coreprof/3",cor_in)
  call euitm_get(idxin,"coreprof",cor_in)
!*call euitm_get(idxin,"coretransp/3",coretransp_in)
  call euitm_get(idxin,"coretransp",coretransp_in)

!  runh  = 200
!  runh  = 300
!  runh = 1101
!  runh = 2761
!  call euitm_open(treename,shot,runh ,idxmhd)  
! call euitm_get(idxmhd,"mhd",mhd_in)
  call euitm_get(idxin,"mhd",mhd_in)

  print *, '**size(mhd_in)',size(mhd_in)
  print *, '**mhd_in%time',mhd_in%time
  print *, '**size(coretransp_in)',size(coretransp_in)
  print *, '**coretransp_in(1)%time',coretransp_in(1)%time

! allocate(mhd_in(1))
  print *, '**size(mhd_in)',size(mhd_in)
!  mhd_in(1)%time=coretransp_in(1)%time
   print *, 'mhd(1)%time',mhd_in(1)%time
   if (associated(mhd_in(1)%n)) then
      if (associated(mhd_in(1)%n(1)%plasma%disp_par)) then
         print *, 'mhd(1)%plasma%par',real(mhd_in(1)%n(1)%plasma%disp_par(1,7))
         print *, 'mhd(1)%plasma%par',real(mhd_in(1)%n(1)%plasma%disp_par(1,8))
      end if
   end if

  jt=size(coretransp_in)
  print *, 'size(coretransp_in)',jt
! allocate(cor_slice(jt))
  allocate(coretransp_out(1))
  allocate(coretransp_out_slice(1))
  print *,'coretransp_in(:)%time= ',coretransp_in(1)%time
  print *,'       mhd_in(:)%time= ',       mhd_in(1)%time

! do i=1,jt
  do i=1,1 

    coretransp_in_slice => coretransp_in(i:i)
    cor_slice => cor_in(i:i)

    ! call euITM_copy_slice2slice_coretransp(coretransp_in(i),coretransp_out_slice(1))
      call euITM_copy(coretransp_in(i),coretransp_out_slice(1))
!   coretransp_out_slice(1)=coretransp_in(i)
    coretransp_out_slice(1)%time=coretransp_in(i)%time
!**   print *,size(coretransp_out(1)%values(1)%ti_transp%diff_eff)

  print *,'** coretransp_in(:)%time= ',coretransp_in(1)%time
  print *,'** coretransp_out(:)%time= ',coretransp_out_slice(1)%time
! print *,'** coretransp_out(:)%time= ',coretransp_out(1)%time

   allocate(mhd_cpo(1))

	    mhd_cpo(1) = mhd_in(i) 
 print *, 'before sub'
    call ntm_DeffTLets(cor_slice, mhd_cpo, coretransp_in_slice, coretransp_out_slice,codeparam_param)
    print *, 'after sub'           
    coretransp_out(i) = coretransp_out_slice(1)
! print *,'**** coretransp_out(:)%time= ',coretransp_out(jt)%time, jt
    !call euITM_copy_slice2slice_coretransp(coretransp_out_slice(1),coretransp_out(i))

    ! write(* ,*) ' coretransp_out(1:5)%time= ',coretransp_out(:)%time
    ! write(* ,*) ' coretransp_out(1:5)%te_transp%diff_eff= ',coretransp_out(1)%te_transp%diff_eff
   print * ,   ' >>coretransp_out(1)%time= ',coretransp_out(1)%time
   print *, ' >>coretransp_out(1)_slice%te_transp%diff_eff= ',coretransp_out_slice(1)%values(1)%te_transp%diff_eff
   print *, ' >>coretransp_out(1)%te_transp%diff_eff= ',coretransp_out(1)%values(1)%te_transp%diff_eff
    write(* ,*) ' >>coretransp_out(1)%ne_transp%diff_eff= ',coretransp_out(1)%values(1)%ne_transp%diff_eff
!   write(* ,*) ' >>coretransp_out(2)%time= ',coretransp_out(2)%time
!   write(* ,*) ' >>coretransp_out(2)%te_transp%diff_eff= ',coretransp_out(2)%te_transp%diff_eff
!   write(* ,*) ' >>coretransp_out(2)%ne_transp%diff_eff= ',coretransp_out(2)%ne_transp%diff_eff

  end do

  write(*,*) 'Creating output run :'
  ! This is the UAL function that creates a shot in the ITM MDS+ tree
  call euitm_create(treename,shot,400   ,  0    ,  0   ,idxout)

  print *, 'runout,refrun',runout,refrun
  call euitm_put(idxout,"coretransp",coretransp_out)

  write(*,*) 'Closing Database :'
  write(20,*) 'Closing Database :'
  call euitm_close(idxin,treename,shot,runin)
  call euitm_close(idxmhd,treename,shot,runh)
  call euitm_close(idxout,treename,shot,400)      

  write(*,*) 'Deallocate CPOs :'
  if (associated(coretransp_in))       call euitm_deallocate(coretransp_in)  
    if (associated(coretransp_out))      call euitm_deallocate(coretransp_out)  
  if (associated(mhd_in))              call euitm_deallocate(mhd_in)  

end program ntm_DeffTL_progETS
