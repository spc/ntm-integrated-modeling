subroutine  ntm_DeffTLets( corep_in, mhd_in, coretransp_in, coretransp_out,params_in)

  use itm_types
  use coeffs_module

  use euITM_schemas
  !use euITM_routines
  ! jorge> using module copy_structures to copy cpo
  use copy_structures
  use interpos_module


  IMPLICIT NONE

  type (type_coretransp), pointer ::     coretransp_in(:), coretransp_out(:)
  type (type_coreprof), pointer ::         corep_in(:)
  type (type_mhd), pointer ::          mhd_in(:)

  type (type_param)  :: params_in       

  real(R8) :: mm, nn, qres, rhotors, width
  real(R8) :: wphase, wfreq, cepsilon, wdt
  real(R8), allocatable :: deffin(:), deffinc(:), Deffw(:), Deffwc(:), xx(:), xxc(:), espx(:), espxx(:)
  real(R8), allocatable :: deffvc(:)
  real(R8), allocatable ::  Deffwn(:), Deffwz(:), Deffwv(:)
  integer :: iunit, ntimes, it, nrho, nrhoc, last_iter, n, m, irhotors(1), istatus, ii
  integer :: itt, ici
  integer :: iwi       
  integer :: kk        
  logical :: dbg = .false.

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   print *, 'in subroutine ntm_DeffTLets' 


! if dbg print *, 'in sub ntm_DeffTLets'

  cepsilon = 1.E-14_r8

! if dbg then
        if (.not. associated(params_in%parameters)) then
                write(0, *) 'ERROR: code parameters not associated!'
                return
        end if
        print *,params_in%parameters
! endif
  ! read values from xml file into variables
  call assign_code_parameters(params_in,istatus)
! if dbg then
        if (istatus /= 0) then
          write(*, *) 'ERROR: Could not assign some code parameters.'
          return
        end if

      print *, 'dopo assign'

! print *, '*coretransp_out%time',coretransp_out(1)%time
! if dbg print *, '*coretransp_in%time',coretransp_in(1)%time

! if dbg print *,'coretransp_in%te_transp%diff_eff= ',coretransp_in(1)%values(1)%te_transp%diff_eff
  print *,'coeff_deff_te= ',coeff_deff_te
  print *,'coeff_width_te= ',coeff_width_te
  print *,'coeff_deff_ti= ',coeff_deff_ti
  print *,'coeff_deff_ni= ',coeff_deff_ni   
  write(42,*) 'coeff_deff_te= ',coeff_deff_te
  write(42,*) 'coeff_deff_ne= ',coeff_deff_ne
  write(42,*) 'coeff_width_te= ',coeff_width_te
  write(42,*) 'coeff_width_ne= ',coeff_width_ne
  write(42,*) 'coeff_deff_ti= ',coeff_deff_ti
  write(42,*) 'coeff_deff_ni= ',coeff_deff_ni   

 ! endif

  ntimes=size(coretransp_in)
  ! allocate(coretransp_out(1))
  ! allocate(coretransp_out(2))
  ! allocate(coretransp_out(ntimes))
  print *,'ntimes= ',ntimes
  write(42,*) 'ntimes= ',ntimes


  !if (.not.associated(coretransp_out)) allocate(coretransp_out(1))
  ! jorge> to be sure that we start with an allocated coretransp out 
  ! with 1 element only (timeslice)
  if (associated(coretransp_out)) deallocate(coretransp_out)
  allocate(coretransp_out(1))
  ! jorge> copy all from coretransp_in to coretransp_out 
  call copy_cpo(coretransp_in(1), coretransp_out(1))

!!call euITM_copy(coretransp_in(1),coretransp_out(1))
! call euITM_copy(coretransp_in,coretransp_out)


       coretransp_out(1)%time = coretransp_in(1)%time
   ! jorge> this would only copy pointers to memory/data and both will share the same  
   ! coretransp_out(1) = coretransp_in(1)
       print *, 'size(coretransp_out)',size(coretransp_out)
       print *, 'coretransp_in(1)%time',coretransp_in(1)%time
!      print *, 'coretransp_out(1)%time',coretransp_out(1)%time
       write(42,*)  'coretransp_in(1)%time',coretransp_in(1)%time

  print *,'size(mhd_in)= ',size(mhd_in)
  ! it=1
  ! jorge> mind that mhd_in is an array, you should do mhd_in(1)%time
  print *, 'mhd_in(1)%time',mhd_in(1)%time
 !print *,'coretransp_in%te_transp%diff_eff= ',coretransp_in(1)%te_transp%diff_eff

  ! do itt=1,size(mhd_in)
  ! do itt=1,ntimes         
  ! do itt=1,2              
  !    nrho=size(coretransp_in(itt)%rho_tor)
 !nrho=size(coretransp_in(ntimes)%rho_tor)
  nrho=size(corep_in(ntimes)%rho_tor)
  itt = 1      
  print *, 'itt,nrho',itt,nrho
  print *,'coretransp_in%te_transp%diff_eff= ',coretransp_in(1)%values(1)%te_transp%diff_eff
  !PH
  !allocate(coretransp_out(itt))
  !coretransp_out(1)%time=coretransp_in(1)%time
  ! coretransp_out(2)%time=coretransp_in(2)%time
  ! print *, '*coretransp_out%time',coretransp_out(1)%time
 print *, '*coretransp_out%time',coretransp_out(1)%time
  
 ! jorge> this inconsistent allocation with respect to what is in compositions is dangerous. For testing is ok but... 
 ! do ici=1,itt
 !         if (.not.associated(coretransp_out(ici)%values)) allocate(coretransp_out(ici)%values(1)) 
 !         if (.not.associated(coretransp_out(ici)%values(1)%te_transp%diff_eff)) & 
 !            allocate(coretransp_out(ici)%values(1)%te_transp%diff_eff(nrho))

 !         if (.not.associated(coretransp_out(ici)%values(1)%ne_transp%diff_eff)) & 
 !            allocate(coretransp_out(ici)%values(1)%ne_transp%diff_eff(nrho,3))

 !         if (.not.associated(coretransp_out(ici)%values(1)%ti_transp%diff_eff)) &
 !            allocate(coretransp_out(ici)%values(1)%ti_transp%diff_eff(nrho,1))

 !         if (.not.associated(coretransp_out(ici)%values(1)%ni_transp%diff_eff)) &
 !           allocate(coretransp_out(ici)%values(1)%ni_transp%diff_eff(nrho,1,3))

 !         if (.not.associated(coretransp_out(ici)%values(1)%nz_transp)) allocate(coretransp_out(ici)%values(1)%nz_transp(1))
 !         if (.not.associated(coretransp_out(ici)%values(1)%nz_transp(1)%diff_eff)) &
 !           allocate(coretransp_out(ici)%values(1)%nz_transp(1)%diff_eff(nrho,74))
 !         if (.not.associated(coretransp_out(ici)%values(1)%nz_transp(1)%vconv_eff)) &
 !           allocate(coretransp_out(ici)%values(1)%nz_transp(1)%vconv_eff(nrho,74))

 ! end do


  print *,'coretransp_in(1)%te_transp%diff_eff= ',coretransp_in(1)%values(1)%te_transp%diff_eff
    

! print *, '*coretransp_out%time',coretransp_out(1)%time
  print *,'coretransp_in(1)%te_transp%diff_eff= ',coretransp_in(1)%values(1)%te_transp%diff_eff
  !do ici=1,nrho
  !  coretransp_out(itt)%te_transp%diff_eff(ici)=coretransp_in(itt)%te_transp%diff_eff(ici)
  !  coretransp_out(itt)%ne_transp%diff_eff(ici,1)=coretransp_in(itt)%te_transp%diff_eff(ici)
  !  coretransp_out(itt)%ti_transp%diff_eff(ici,1)=coretransp_in(itt)%te_transp%diff_eff(ici)
  !  coretransp_out(itt)%ni_transp%diff_eff(ici,1,1)=coretransp_in(itt)%te_transp%diff_eff(ici)
  !enddo

  print *,'>itt, coretransp_out(it)%te_transp%diff_eff',itt, coretransp_out(itt)%values(1)%te_transp%diff_eff
  print *,'>itt, coretransp_out(it)%ne_transp%diff_eff',itt, coretransp_out(itt)%values(1)%ne_transp%diff_eff(:,2)

! print *,'>itt, coretransp_out(it)%nz_transp%diff_eff',itt, coretransp_out(itt)%values(1)%nz_transp(1)%diff_eff(:,31)
! print *,'>itt, coretransp_out(it)%nz_transp%vconv_eff',itt, coretransp_out(itt)%values(1)%nz_transp(1)%vconv_eff(:,31)


! print *,'size(mhd_in(itt)%disp_par,1)= ',size(mhd_in(itt)%plasma%disp_par,1)
! print *,'size(mhd_in(itt)%disp_par,2)= ',size(mhd_in(itt)%plasma%disp_par,2)
! print *,'size(mhd_in(itt)%disp_par,3)= ',size(mhd_in(itt)%plasma%disp_par,3)
! print *,'size(mhd_in(itt)%n)= ',size(mhd_in(itt)%n)
! print *, mhd_in(itt)%n(1)
! print *, mhd_in(itt)%plasma%m(1,1,1)
! print *, mhd_in(itt)%frequency(1)
! print *, 'width',mhd_in(itt)%plasma%disp_par(size(mhd_in(itt)%plasma%disp_par,1),1,1)
  ! end do

  ! do it=1,1
  !    it=itt-1
  it=itt
  print *,'it= ',it    
  nrhoc=size(coretransp_in(it)%values(1)%rho_tor)
  print *,'nrhoc= ',nrhoc
  allocate(deffinc(nrhoc))
  allocate(Deffwc(nrhoc))
  allocate(Deffwz(nrhoc))
  allocate(Deffvc(nrhoc))
  allocate(xxc(nrhoc))

  nrho=size(corep_in(it)%rho_tor)
  print *,'nrho= ',nrho
  allocate(deffin(nrho))
  allocate(Deffw(nrho))
  allocate(Deffwn(nrho))
  allocate(xx(nrho))
  allocate(espx(nrho))
  allocate(espxx(nrho))

    write(42,*) 'nrhoc',nrhoc
    write(42,*) 'nrho ',nrho 

  !    coretransp_out(it)%time=real(it,R8)
! print *,' coretransp_out(it)%time= ',coretransp_out(it)%time
! last_iter=size(mhd_in(it)%plasma%disp_par,1)
! print *,'last_iter= ',last_iter
! n=mhd_in(it)%n(1)
! m=mhd_in(it)%plasma%m(1,n,1)
!  n=1
!  m=2

  if (associated(mhd_in(it)%n)) then
     nn = mhd_in(it)%n(1)%modenum 
     if (associated(mhd_in(it)%n(1)%plasma%m)) then
        print *,'mhd_in(it)%n(1), mhd_in(it)%n(1)%plasma%m(1,1)'
        mm = mhd_in(it)%n(1)%plasma%m(1,1)
     else
	!jorge >  mhd_in is an input CPO. It should not be touch
        !allocate(mhd_in(it)%n(1)%plasma%m(1,1))
        !mm = 2
        !mhd_in(it)%n(1)%plasma%m(1,1) = mm
        continue
	!jorge 
     end if
  else
	!jorge>  mhd_in is an input CPO. It should not be touch
	!     allocate(mhd_in(it)%n(1))
	!     allocate(mhd_in(it)%n(1)%plasma%m(1,1))
	!     mm = 2
	!     nn = 1
	!     mhd_in(it)%n(1)%modenum = nn
	!     mhd_in(it)%n(1)%plasma%m(1,1) = mm
        continue
	!jorge> 
  end if
!  rewind 88
!  write(88,*) mhd_in(it)%plasma%m(1,1,1), mhd_in(it)%n
!  rewind 88
!  read(88,*) mm, nn
!  write(88,*) mm, nn

! qres=real(mm,R8) / real(nn,R8)
! qres=mm / mhd_in(it)%n
  qres=mm / nn              
!j write(88,*) 'in ntmDeff n,m', mhd_in(it)%n(1), mhd_in(it)%plasma%m(1,1,1)
!j write(88,*) 'in ntmDeff n,m,qres', mm, nn, qres                                 
  !    rhotors=mhd_in(1)%disp_par(last_iter,n,7)
  !    width=mhd_in(1)%disp_par(last_iter,n,1)
  
  print *, 'mhd_in%time',mhd_in(it)%time
! print *, 'par20,1,7',mhd_in(1)%plasma%disp_par%re(20,1,7)

! rhotors=mhd_in(it)%plasma%disp_par(1,n,7)
!!rhotors=mhd_in(it)%plasma%disp_par(20,1,7)
!   rhotors=0.29
!   rhotors=0.8 
! print *,'*rhotors1',rhotors
! rhotors=mhd_in(it)%disp_par(last_iter,n,7)
! print *,'*rhotors2',rhotors
! width=mhd_in(it)%plasma%disp_par(last_iter,n,1)

  last_iter=1
! do iwi=2,500
  do iwi=1,500
!    if (mhd_in(it)%plasma%disp_par%re(iwi,n,1) > 0) last_iter=iwi
!!   if (mhd_in(it)%plasma%disp_par%re(iwi,1,1) > 0) last_iter=iwi
     if (associated(mhd_in(it)%n(1)%plasma%disp_par)) then
        if (real(mhd_in(it)%n(1)%plasma%disp_par(iwi,1)) > 0) last_iter=iwi
     else
        last_iter = iwi
     end if
!    print *, '***',  iwi, last_iter, mhd_in(it)%plasma%disp_par%re(iwi,n,1) 
!    print *, '***',  iwi, last_iter, mhd_in(it)%plasma%disp_par%re(iwi,1,1) 
  enddo
! width=mhd_in(it)%plasma%disp_par(20,       n,1)
! width=mhd_in(it)%plasma%disp_par%re(last_iter,n,1)
! rhotors=mhd_in(it)%plasma%disp_par%re(last_iter,n,7)
  if (associated(mhd_in(it)%n(1)%plasma%disp_par)) then
     width=real(mhd_in(it)%n(1)%plasma%disp_par(last_iter,1))
     rhotors=real(mhd_in(it)%n(1)%plasma%disp_par(last_iter,7))
     wphase=real(mhd_in(1)%n(1)%plasma%disp_par(last_iter,2))
     wfreq=real(mhd_in(1)%n(1)%plasma%disp_par(last_iter,3))
  else
     ! test values
     width = 0.02_r8
     rhotors = 0.7_r8
     wphase = 0._r8
     wfreq = 10.e3_r8
  end if
  print *, 'last_iter',last_iter
  print *, 'width mhd',width
  print *,'rhotors',rhotors
  write(42,*) 'last_iter',last_iter
  write(42,*)  'width mhd',width
  write(42,*) 'rhotors',rhotors
  write(20,*) 'rhotors',rhotors

  ! construct added Deff

  ! Te
  print *,'size(coretransp_in)= ',size(coretransp_in)
  if (.not. associated(coretransp_in(it)%values(1)%te_transp%diff_eff) .or. size(coretransp_in).eq.0 ) then
    print *,'coretransp_in(it)%te_transp%diff_eff not associated'
    write(12,*) 'coretransp_in(it)%te_transp%diff_eff not associated'
!   return
     goto 2000
  end if
     print *, 'size rhotor', size(corep_in(it)%rho_tor)
  print *,'it, corep_in(it)%rho_tor',it, corep_in(it)%rho_tor              
  print *,'it, coretransp_in(it)%te_transp%diff_eff',it, coretransp_in(it)%values(1)%te_transp%diff_eff
  print *,'size te_transp', size(coretransp_in(it)%values(1)%te_transp%diff_eff)
  deffin =  coretransp_in(it)%values(1)%te_transp%diff_eff
  print *, 'deffin',  coretransp_in(it)%values(1)%te_transp%diff_eff
  write(42,*) 'deffin',  coretransp_in(it)%values(1)%te_transp%diff_eff
!  rhotors=rhotors/100.
  print *,'core%rho',corep_in(it)%rho_tor(:)
  print *, 'rhotors',rhotors
  xx=2.0*abs(corep_in(it)%rho_tor-rhotors)
  print *, '----->xx',xx  
! irhotors(1)=minloc(corep_in(it)%rho_tor,rhotors)
  irhotors(1)=minloc(xx,1)                            
  print *,'rhotors, width, qres, irhotors= ',rhotors, width, qres, irhotors
  print *,'irhotors= ',irhotors
  print *, 'width Deffw',width
  write(42,*) 'rhotors, width, qres, irhotors= ',rhotors, width, qres, irhotors
  write(42,*) 'irhotors= ',irhotors
  write(42,*) 'width Deffw',width
  write(20,*)  'coeff_width_te',coeff_width_te
  write(20,*)  'width Deffw',width
!       coeff_width_te = 2.0
!       coeff_deff_te = 5.0
  Deffw=coeff_deff_te * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
! Deffw=                deffin(irhotors(1)) * exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *, 'espd', (coeff_width_te*width)**2
  espx =  exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *, 'espe', exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *,'Deffin',deffin(irhotors(1))
  print *, 'deffin*exp',deffin(irhotors(1)) * exp(-xx**2/(coeff_width_te*width+cepsilon)**2)
  print *,'Deffw_Te= ',Deffw
  !    allocate(coretransp_out(it)%te_transp%diff_eff(nrho))
  if(width .le. 0.0) wdt=0.0
  if(width .gt. 0.0) wdt=1.0
! coretransp_out(it)%values(1)%te_transp%diff_eff = deffin + Deffw * 1.0  
  coretransp_out(it)%values(1)%te_transp%diff_eff = deffin + Deffw * wdt  
! coretransp_out(it)%te_transp%diff_eff = deffin 


 !print *, 'deffin+Deffw', deffin, Deffw, deffin+Deffw                             

2000   continue
!           goto 2000
  ! ne
  if (.not. associated(coretransp_in(it)%values(1)%ne_transp%diff_eff) .or. size(coretransp_in).eq.0 ) then
    print *,'coretransp_in(it)%ne_transp%diff_eff not associated'
    write(12,*) 'coretransp_in(it)%ne_transp%diff_eff not associated'
!   return
     goto 2100
  end if
  ! print *,'coretransp_in(it)%ne_transp%diff_eff(1,:)',coretransp_in(it)%ne_transp%diff_eff(1,:)
  ! deffin =  coretransp_in(it)%values(1)%ne_transp%diff_eff(:,1)
    deffin =  coretransp_in(it)%values(1)%ne_transp%diff_eff(:,2)
    write(42,*) 'ne  deffin', deffin
  write(42,*)  'in1_coretransp_ne',coretransp_in(it)%values(1)%ne_transp%diff_eff(:,1)
  write(42,*)
  write(42,*)  'in2_coretransp_ne',coretransp_in(it)%values(1)%ne_transp%diff_eff(:,2)
  write(42,*)
  write(42,*)  'in3_coretransp_ne',coretransp_in(it)%values(1)%ne_transp%diff_eff(:,3)
  write(42,*)
  
 !print *,'coretransp_in(it)%rho_tor(:)= ',coretransp_in(it)%rho_tor(:)
 !print *,'rhotors= ',rhotors
! xx=abs(coretransp_in(it)%rho_tor(:)-rhotors)
  xx=2.0*abs(corep_in(it)%rho_tor-rhotors)
! irhotors(1)=minloc(xx,1)
  write(42,*)  'coeff_deff_ne ',coeff_deff_ne    
  write(42,*)  'coeff_width_ne',coeff_width_ne
  write(*,*)  'coeff_deff_ne ',coeff_deff_ne    
  write(*,*)  'coeff_width_ne',coeff_width_ne
  Deffwn=coeff_deff_ne * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
 !print *,'Deffw= ',Deffw
  espxx=  exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
!      allocate(coretransp_out(it)%ne_transp%diff_eff(nrho,3))
! coretransp_out(it)%values(1)%ne_transp%diff_eff(:,1) = deffin + Deffw
! coretransp_out(it)%values(1)%ne_transp%diff_eff(:,1) = deffin + Deffwn*wdt
  coretransp_out(it)%values(1)%ne_transp%diff_eff(:,2) = deffin + Deffwn          
  write(42,*)  'Out2_coretransp_ne',coretransp_out(it)%values(1)%ne_transp%diff_eff(:,2)

2100   continue

!   goto 2000

  ! Ti
  if (.not. associated(coretransp_in(it)%values(1)%ti_transp%diff_eff) .or. size(coretransp_in).eq.0 ) then
    print *,'coretransp_in(it)%ti_transp%diff_eff not associated'
    write(12,*) 'coretransp_in(it)%ti_transp%diff_eff not associated'
!   return
     goto 2200
  end if
       print *,'coretransp_in(it)%ti_transp%diff_eff(1,:)= ',coretransp_in(it)%values(1)%ti_transp%diff_eff(1,:)
       deffinc= coretransp_in(it)%values(1)%ti_transp%diff_eff(:,1)
  print *,'deffin Ti= ',deffinc
  xxc=2.0*abs(coretransp_in(it)%values(1)%rho_tor(:)-rhotors)
! xx=2.0*abs(corep_in(it)%rho_tor-rhotors)
  print *, 'XXc_Ti ', xxc
  irhotors(1)=minloc(xxc,1)                            
   write(*,*) '==>> irhotors(1)), rhotors',irhotors(1),rhotors
!   deffin = coretransp_in(it)%values(1)%ti_transp%diff_eff(:,1)
  Deffwc=coeff_deff_ti * deffinc(irhotors(1)) * exp(-xxc**2/(coeff_width_te*width+cepsilon)**2)
!      allocate(coretransp_out(it)%ti_transp%diff_eff(nrho,1))
!   deffin = coretransp_in(it)%ti_transp%diff_eff(:,1)
! coretransp_out(it)%values(1)%ti_transp%diff_eff(:,1) = deffinc + Deffwc
  coretransp_out(it)%values(1)%ti_transp%diff_eff(:,1) = deffinc + Deffwc *wdt
!      print *,'coretransp_out(it)%ti_transp%diff_eff(:,1)= ',coretransp_out(it)%values(1)%ti_transp%diff_eff(:,1)

2200   continue

  ! ni
  if (.not. associated(coretransp_in(it)%values(1)%ni_transp%diff_eff) .or. size(coretransp_in).eq.0 ) then
    print *,'coretransp_in(it)%ni_transp%diff_eff not associated'
    write(12,*) 'coretransp_in(it)%ni_transp%diff_eff not associated'
!   return
     goto 2300
  end if
       print *,'coretransp_in(it)%ni_transp%diff_eff(:,1,1)',coretransp_in(it)%values(1)%ni_transp%diff_eff(:,1,1)
       print *, 'ni_transp'
       deffinc= coretransp_in(it)%values(1)%ni_transp%diff_eff(:,1,1)
       print *, 'Deffinc_ni',deffinc
  xxc=2.0*abs(coretransp_in(it)%values(1)%rho_tor(:)-rhotors)
  print *, 'XXc_ni ', xxc
 !xx=abs(coretransp_in(it)%rho_tor(:)-rhotors)
  irhotors(1)=minloc(xxc,1)
   write(*,*) '==> irhotors(1)), rhotors',irhotors(1),rhotors
   Deffwc=coeff_deff_ni * deffin(irhotors(1)) * exp(-xxc**2/(coeff_width_ne*width+cepsilon)**2)
  !    allocate(coretransp_out(it)%ni_transp%diff_eff(nrho,1,3))
! coretransp_out(it)%values(1)%ni_transp%diff_eff(:,1,3) = deffinc + Deffwc
! coretransp_out(it)%values(1)%ni_transp%diff_eff(:,1,3) = deffinc + Deffwc * wdt
  coretransp_out(it)%values(1)%ni_transp%diff_eff(:,1,1) = deffinc                  
!      print *,'coretransp_out(it)%ni_transp%diff_eff(:,1,3)= ',coretransp_out(it)%values(1)%ni_transp%diff_eff(:,1,3)
       print *, 'end Ti ni'


2300   continue

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! nz
    write(42,*) 'coeff_deff_ni, coeff_width_ne,width'
    write(42,*)  coeff_deff_ni, coeff_width_ne,width 
  print *,'size(coretransp_in)= ',size(coretransp_in)
  if (.not. associated(coretransp_in(it)%values(1)%nz_transp(1)%diff_eff) .or. size(coretransp_in).eq.0 ) then
    print *,'coretransp_in(it)%nz_transp%diff_eff not associated'
    write(12,*) ,'coretransp_in(it)%nz_transp%diff_eff not associated'
!   return
     goto 2400
  end if
     print *, 'size rhotor', size(corep_in(it)%rho_tor)
! print *,'it, corep_in(it)%rho_tor',it, corep_in(it)%rho_tor              
! print *,'it, coretransp_in(it)%nz_transp%diff_eff',it, coretransp_in(it)%values(1)%te_transp%diff_eff
! print *,'size te_transp', size(coretransp_in(it)%values(1)%te_transp%diff_eff)
! deffin =  coretransp_in(it)%values(1)%nz_transp(1)%diff_eff(:,74)

    do kk=1,74  

    do ii=1,100
! deffin(ii) =  coretransp_in(it)%values(1)%nz_transp(1)%vconv_eff(ii,kk)
  deffin(ii) =  coretransp_in(it)%values(1)%nz_transp(1)%diff_eff(ii,kk)
  deffvc(ii) =  coretransp_in(it)%values(1)%nz_transp(1)%vconv_eff(ii,kk)
    enddo
    do ii=1,100
    if(deffin(ii) .le. 0.0) deffin(ii)=1.0e-05
    if(deffvc(ii) .le. 0.0) deffvc(ii)=1.0e-05
    enddo
! print *, 'deffin',  coretransp_in(it)%values(1)%te_transp%diff_eff
! write(42,*) 'nz_deffin',  coretransp_in(it)%values(1)%nz_transp(1)%vconv_eff(:,45)
  write(44,*) 'it',it
  write(44,*) 'nz_deffin',coretransp_in(it)%values(1)%rho_tor,coretransp_in(it)%values(1)%nz_transp(1)%diff_eff(:,45)
  write(44,*) 'nz_deffvc',coretransp_in(it)%values(1)%rho_tor,coretransp_in(it)%values(1)%nz_transp(1)%vconv_eff(:,45)
  write(44,*)
!  rhotors=rhotors/100.
  print *,'core%rho',corep_in(it)%rho_tor(:)
  print *, 'rhotors',rhotors
   write(42,*) 'rhotors', rhotors
  xx=2.0*abs(corep_in(it)%rho_tor-rhotors)
  print *, '----->xx',xx  
! irhotors(1)=minloc(corep_in(it)%rho_tor,rhotors)
  irhotors(1)=minloc(xx,1)                            
  print *,'rhotors, width, qres, irhotors= ',rhotors, width, qres, irhotors
  print *,'irhotors= ',irhotors
  print *, 'width Deffw',width
  write(42,*) 'rhotors, width, qres, irhotors= ',rhotors, width, qres, irhotors
  write(42,*) 'irhotors= ',irhotors
  write(42,*) 'width Deffw',width
  write(20,*)  'coeff_width_nz',coeff_width_ne
  write(20,*)  'width Deffw',width
!       coeff_width_te = 2.0
!       coeff_deff_te = 5.0
    write(42,*) 'coeff_deff_ni',coeff_deff_ni
    write(42,*) 'coeff_width_ne',coeff_width_ne    
  Deffwz=     coeff_deff_ni * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
! Deffwz=100.*coeff_deff_ni * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
! Deffwz=100.*5000000.      * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
  Deffwv=-coeff_deff_other * deffvc(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
! Deffwv=-10000.*coeff_deff_ni * deffvc(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
! Deffwv=-1000.*50000000.         * deffvc(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
! Deffw=                deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
  print *, 'espd', (coeff_width_te*width)**2
  espxx=  exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
  print *, 'espe', exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
  print *,'Deffin',deffin(irhotors(1))
  print *, 'deffin*exp',deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width+cepsilon)**2)
  print *,'Deffw_nz= ',Deffwz
  !    allocate(coretransp_out(it)%te_transp%diff_eff(nrho))
  if(width .le. 0.0) wdt=0.0
  if(width .gt. 0.0) wdt=1.0
! coretransp_out(it)%values(1)%nz_transp(1)%diff_eff(:,74) = deffin + Deffw * wdt  
! coretransp_out(it)%values(1)%nz_transp(1)%vconv_eff(:,74) = deffin + Deffw * wdt  
    write(42,*) 'WIDTH, WDT', width, wdt
   do ii=1,100
! coretransp_out(it)%values(1)%nz_transp(1)%vconv_eff(ii,kk) = deffin(ii) + Deffwz(ii)* wdt  
  coretransp_out(it)%values(1)%nz_transp(1)%diff_eff(ii,kk) = deffin(ii) + Deffwz(ii)* wdt  
  coretransp_out(it)%values(1)%nz_transp(1)%vconv_eff(ii,kk) = deffvc(ii) + Deffwv(ii)* wdt  
  write(42,*) 'do_nz', ii,kk,deffin(ii),Deffwz(ii),width, wdt,  &  
  coretransp_out(it)%values(1)%nz_transp(1)%diff_eff(ii,kk),  &
  coretransp_out(it)%values(1)%nz_transp(1)%vconv_eff(ii,kk) 
    enddo

    enddo


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  2000   continue
   2400   continue

  do ii=1,nrho                  
 !  write(42,42) ii,coretransp_in(it)%rho_tor(ii),espx(ii),coretransp_in(it)%te_transp%diff_eff(ii), & 
    write(42,42) ii,corep_in(it)%rho_tor(ii),coretransp_in(it)%values(1)%te_transp%diff_eff(ii), & 
     coretransp_out(it)%values(1)%te_transp%diff_eff(ii), Deffw(ii),espx(ii),    &
!    coretransp_out(it)%values(1)%nz_transp(1)%vconv_eff(ii,45),              &
!    coretransp_out(it)%values(1)%nz_transp(1)%vconv_eff(ii,25),              &
!    coretransp_out(it)%values(1)%nz_transp(1)%diff_eff(ii,25), Deffwz(ii),  &
     espxx(ii)

   if(Deffw(ii)  <= 1.0e-99) Deffw(ii)=1.e-99
   if(espx(ii)  <= 1.0e-99) espx(ii)=1.e-99
   if(espxx(ii)  <= 1.0e-99) espxx(ii)=1.e-99
!  if(Deffwc(ii)  <= 1.0e-99) Deffwc(ii)=1.e-99
!  write(*,*) 'irhotors(1))',irhotors(1)
 !CAD2014 write(42,42) ii,corep_in(it)%rho_tor(ii),espx(ii),Deffw(ii),coretransp_in(it)%values(1)%te_transp%diff_eff(ii), & 
 !   coretransp_out(it)%values(1)%te_transp%diff_eff(ii),corep_in(it)%te%value(ii),width,   &
 !   corep_in(it)%time,mhd_in(it)%time,coretransp_in(it)%time,coretransp_out(it)%time,              &
 !       deffin(irhotors(1)), coeff_width_te, coeff_deff_te
 !   coretransp_out(it)%ne_transp%diff_eff(ii,1),coretransp_out(it)%ti_transp%diff_eff(ii,1)
 !   coretransp_out(it)%ni_transp%diff_eff(ii,1,1)
  enddo

!2000    continue         

  print *, 'end of Subroutine it',it

  ! end do
 42   format(i5,16(1pe12.4))

    rewind 42
    !rewind 88

  deallocate(deffinc)
  deallocate(Deffwc)
  deallocate(xxc)

  deallocate(deffin)
  deallocate(Deffw)
  deallocate(xx)
  deallocate(espx)
  deallocate(espxx)

  print *, 'end of ntm_DeffTLets routine'


end subroutine ntm_DeffTLets
