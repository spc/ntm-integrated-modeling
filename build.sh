#!/bin/bash -fe

cd .
make clean
make libntmDeffTLets

# can test with: fc2k -kepler -docfile doc/actor_name.txt fc2k/actor_name.xml (for each actor_name in fc2k)
echo " "
echo "Can execute: module switch fc2k/rc; fc2k -kepler -docfile doc/ntmDeffTLets.txt fc2k/ntmDeffTLets.xml"
