PROG     = ntm_DeffTL_progETS
libPROG  = libntmDeffTLets

ifeq ($(ITM_ENVIRONMENT_LOADED),yes)
  DATAMODEL=ITM
else
  DATAMODEL=IMAS
endif

MODS        = itm_types.f90 coeffs_module.f90 string_manipulation_tools.f90

ifeq ($(DATAMODEL),ITM)
  MODS += euitm_xml_parser.f90
  SOURCESlib_datamodel  = ntm_DeffTLets.f90 assign_code_parameters.f90
  SOURCES_datamodel  = ntm_DeffTL_progETS.f90 $(SOURCESlib_datamodel)
else
  MODS += ids_xml_parser.f90
  SOURCESlib_datamodel  =  ntm_DeffTLets_imas.f90 assign_code_parameters_imas.f90
  SOURCES_datamodel  = ntm_DeffTL_progETS_imas.f90 $(SOURCESlib_datamodel)
endif

SOURCES_all = 

SOURCESlib = $(SOURCESlib_datamodel) $(SOURCES_all)
SOURCES	   = $(SOURCES_datamodel) $(SOURCES_all)

OBJS        = $(MODS:.f90=.o) $(SOURCES:.f90=.o)
OBJSlib     = $(MODS:.f90=.o) $(SOURCESlib:.f90=.o)

.PRECIOUS:  $(MODS) $(SOURCES)

#
FCOMPILER = ifort
FC         = $(FCOMPILER)
FFLAG    = -r8 -fPIC  -fpp -O3 -g -C -assume no2underscore
FFLAG    = -r8 -fPIC  -fpp -O0 -g -C -assume no2underscore -traceback -warn all -ftrapuv

#LIBS    = $(shell eval-pkg-config --libs   lapack-${OBJECTCODE} ual-${OBJECTCODE} xmllib-${OBJECTCODE} itmconstants-${OBJECTCODE} itmtypes-${OBJECTCODE} interpos-${OBJECTCODE}) -lm 
#INCLUDE = $(shell eval-pkg-config --cflags lapack-${OBJECTCODE} ual-${OBJECTCODE} xmllib-${OBJECTCODE} itmconstants-${OBJECTCODE} itmtypes-${OBJECTCODE} interpos-${OBJECTCODE}) 

ifeq ($(DATAMODEL),ITM)
  PACKAGES = ual-$(FCOMPILER) itmtypes-$(FCOMPILER) xmllib-$(FCOMPILER) itmconstants-$(FCOMPILER) interpos-$(FCOMPILER)
else
  PACKAGES = imas-$(FCOMPILER) xmllib interpos
endif

INCLUDE+=$(shell pkg-config --cflags $(PACKAGES))
LIBS+=   $(shell pkg-config --libs   $(PACKAGES) slatec-${FCOMPILER})  -lm  -lz

$(PROG) : $(OBJS)
	$(FC) $(FFLAG) -o $@ $^ $(INCLUDE) $(LIBS)  

library: $(libPROG)

$(libPROG):   $(OBJSlib)
	$(FC) $(FFLAG) -c $(OBJSlib:.o=.f90)  $(INCLUDE) $(LIBS)  
	ar -rv libntmDeffTLets.a $(OBJSlib)  *.o


source_full : 
	cat $(MODS) $(SOURCES) > $(PROG)_full.f90

clean : 
	rm -f $(PROG) $(OBJS) $(OBJSlib) *.mod *.o *.a



ntm_DeffTL_progETS.o : ntm_DeffTL_progETS.f90 itm_types.o
	$(FC) $(FFLAG) -c $(@:.o=.f90)       $(INCLUDE) $(LIBS)  
ntm_DeffTL_progETS_imas.o : ntm_DeffTL_progETS_imas.f90 itm_types.o
	$(FC) $(FFLAG) -c $(@:.o=.f90)    $(INCLUDE) $(LIBS)  
ntm_DeffTLets.o : ntm_DeffTLets.f90 itm_types.o coeffs_module.o
	$(FC) $(FFLAG) -c $(@:.o=.f90)             $(INCLUDE) $(LIBS)           
ntm_DeffTLets_imas.o : ntm_DeffTLets_imas.f90 itm_types.o coeffs_module.o
	$(FC) $(FFLAG) -c $(@:.o=.f90)             $(INCLUDE) $(LIBS)           
coeffs_module.o :
	$(FC) $(FFLAG) -c $(@:.o=.f90)             $(INCLUDE) $(LIBS)     
assign_code_parameters.o :
	$(FC) $(FFLAG) -c $(@:.o=.f90)    $(INCLUDE) $(LIBS)
assign_code_parameters_imas.o : ids_xml_parser.f90 assign_code_parameters_imas.f90
	$(FC) $(FFLAG) $(INCLUDE) -c $(@:.o=.f90)
euitm_xml_parser.o :
	$(FC) $(FFLAG) -c $(@:.o=.f90)          $(INCLUDE) $(LIBS)   
ids_xml_parser.o : ids_xml_parser.f90
	$(FC) $(FFLAG) $(INCLUDE) -c $(@:.o=.f90)
itm_types.o :
	$(FC) $(FFLAG) -c $(@:.o=.f90)                 $(INCLUDE) $(LIBS)         
string_manipulation_tools.o :
	$(FC) $(FFLAG) -c $(@:.o=.f90) $(INCLUDE) $(LIBS)

echo:
	@echo LIBS $(LIBS)
	@echo INCLUDE $(INCLUDE)
	@echo SOURCES $(SOURCES)

