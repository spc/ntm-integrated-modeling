subroutine  ntm_Deff( mhd_in, coretransp_in, coretransp_out,params_in)

  use itm_types
  use coeffs_module

  use euITM_schemas
  USE interpos_module


  IMPLICIT NONE

  type (type_coretransp), pointer ::     coretransp_in(:), coretransp_out(:)
  type (type_mhd), pointer ::          mhd_in(:)

  type (type_param)  :: params_in       
  
  real(R8) :: qres, rhotors, width
  real(R8), allocatable :: deffin(:), Deffw(:), xx(:)
  integer :: iunit, ntimes, it, nrho, last_iter, m, n, irhotors(1), istatus, ii
  integer :: itt, ici

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  if (.not. associated(params_in%parameters)) then
    write(0, *) 'ERROR: code parameters not associated!'
    return
  end if
  print *,params_in%parameters
  ! read values from xml file into variables
  call assign_code_parameters(params_in,istatus)
  if (istatus /= 0) then
    write(*, *) 'ERROR: Could not assign some code parameters.'
    return
  end if

  print *,'coeff_deff_te= ',coeff_deff_te
  print *,'coeff_width_te= ',coeff_width_te

  ntimes=size(coretransp_in)
! allocate(coretransp_out(1))
  allocate(coretransp_out(2))
  print *,'ntimes= ',ntimes
  print *,'size(mhd_in)= ',size(mhd_in)
! it=1
   print *, 'mhd_in%time',mhd_in%time

  coretransp_out(1)%time=coretransp_in(1)%time
  coretransp_out(2)%time=coretransp_in(2)%time
   print *, '*coretransp_out%time',coretransp_out%time

! do itt=1,size(mhd_in)
! do itt=1,ntimes         
  do itt=1,2              
     nrho=size(coretransp_in(itt)%rho_tor)
      print *, 'itt,nrho',itt,nrho
     allocate(coretransp_out(itt)%te_transp%diff_eff(nrho))
     allocate(coretransp_out(itt)%ne_transp%diff_eff(nrho,3))
     allocate(coretransp_out(itt)%ti_transp%diff_eff(nrho,1))
     allocate(coretransp_out(itt)%ni_transp%diff_eff(nrho,1,3))
                do ici=1,nrho
      coretransp_out(itt)%te_transp%diff_eff(ici)=coretransp_in(itt)%te_transp%diff_eff(ici)
      coretransp_out(itt)%ne_transp%diff_eff(ici,3)=coretransp_in(itt)%te_transp%diff_eff(ici)
      coretransp_out(itt)%ti_transp%diff_eff(ici,1)=coretransp_in(itt)%te_transp%diff_eff(ici)
      coretransp_out(itt)%ni_transp%diff_eff(ici,1,3)=coretransp_in(itt)%te_transp%diff_eff(ici)
                enddo
   print *,'>itt, coretransp_out(it)%te_transp%diff_eff',itt, coretransp_out(itt)%te_transp%diff_eff
   print *,'>itt, coretransp_out(it)%ne_transp%diff_eff',itt, coretransp_out(itt)%ne_transp%diff_eff(:,3)
  print *,'size(mhd_in(it)%disp_par,1)= ',size(mhd_in(itt)%disp_par,1)
  print *,'size(mhd_in(it)%disp_par,2)= ',size(mhd_in(itt)%disp_par,2)
  print *,'size(mhd_in(it)%disp_par,3)= ',size(mhd_in(itt)%disp_par,3)
  print *,'size(mhd_in(it)%n)= ',size(mhd_in(itt)%n)
  print *, mhd_in(itt)%n(1)
  print *, mhd_in(itt)%m(1,1)
  print *, mhd_in(itt)%frequency(1)
  print *, 'width',mhd_in(itt)%disp_par(size(mhd_in(itt)%disp_par,1),1,1)
  end do

! do it=1,1
     it=itt-1
     print *,'it= ',it    
     nrho=size(coretransp_in(it)%rho_tor)
     print *,'nrho= ',nrho
     allocate(deffin(nrho))
     allocate(Deffw(nrho))
     allocate(xx(nrho))
!    coretransp_out(it)%time=real(it,R8)
     print *,' coretransp_out(it)%time= ',coretransp_out(it)%time
     last_iter=size(mhd_in(it)%disp_par,1)
     print *,'last_iter= ',last_iter
     n=mhd_in(it)%n(1)
     m=mhd_in(it)%m(n,1)
     print *,'n,m= ',n,m
     qres=real(m,R8) / real(n,R8)
!    rhotors=mhd_in(1)%disp_par(last_iter,n,7)
!    width=mhd_in(1)%disp_par(last_iter,n,1)
     rhotors=mhd_in(it)%disp_par(last_iter,n,7)
     width=mhd_in(it)%disp_par(last_iter,n,1)
     print *,'*rhotors',rhotors
     print *,'*width  ',width   
     width=width*100.
!    rhotors=16.5
     print *, 'width',width
     print *,'rhotors',rhotors
      write(20,*) 'rhotors',rhotors
     !wphase=mhd_in(1)%disp_par(last_iter,n,2)
     !wfreq=mhd_in(1)%disp_par(last_iter,n,3)

     ! construct added Deff
     
     ! Te
      print *,'size(coretransp_in)= ',size(coretransp_in)
      if (.not. associated(coretransp_in(it)%te_transp%diff_eff) .or. size(coretransp_in).eq.0 ) then
        print *,'coretransp_in(it)%te_transp%diff_eff not associated'
        return
      end if
      print *,'it, coretransp_in(it)%rho_tor',it, coretransp_in(it)%rho_tor            
      print *,'it, coretransp_in(it)%te_transp%diff_eff',it, coretransp_in(it)%te_transp%diff_eff
     deffin = coretransp_in(it)%te_transp%diff_eff
      rhotors=rhotors/100.
     xx=abs(coretransp_in(it)%rho_tor(:)-rhotors)
!    xx=abs(coretransp_in(it)%rho_tor(:)/6.7-rhotors)  !/6.7 to have rhot ~ rhot of FTU ONLY for te_transp
     print *, '----->xx',xx  
     irhotors(1)=minloc(xx,1)
           print *,'rhotors, width, qres, irhotors= ',rhotors, width, qres, irhotors
     Deffw=coeff_deff_te * deffin(irhotors(1)) * exp(-xx**2/(2*coeff_width_te*width)**2)
!    allocate(coretransp_out(it)%te_transp%diff_eff(nrho))
     coretransp_out(it)%te_transp%diff_eff = deffin + Deffw

     do ii=1,nrho                  
     write(41,41) ii,coretransp_in(it)%rho_tor(ii)/6.7,deffin(ii),Deffw(ii),deffin(ii)+Deffw(ii)
     write(42,41) ii,coretransp_in(it)%rho_tor(ii)    ,deffin(ii), coretransp_out(it)%te_transp%diff_eff(ii)
     enddo
 41  format(i5,4(1pe12.4))

  print *, 'deffin+Deffw', deffin, Deffw, deffin+Deffw                             

     ! ne
!    print *,'coretransp_in(it)%ne_transp%diff_eff(1,:)',coretransp_in(it)%ne_transp%diff_eff(1,:)
     ! deffin = coretransp_in(it)%ne_transp%diff_eff(:,2)
     deffin = deffin
     xx=abs(coretransp_in(it)%rho_tor(:)-rhotors)
     irhotors(1)=minloc(xx,1)
     Deffw=coeff_deff_ne * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width)**2)
!    allocate(coretransp_out(it)%ne_transp%diff_eff(nrho,3))
     coretransp_out(it)%ne_transp%diff_eff(:,3) = deffin + Deffw
     ! Ti
!    print *,'coretransp_in(it)%ti_transp%diff_eff(1,:)= ',coretransp_in(it)%ti_transp%diff_eff(1,:)
!    deffin = coretransp_in(it)%ti_transp%diff_eff(:,1)
     print *,'deffin Ti= ',deffin
     xx=abs(coretransp_in(it)%rho_tor(:)-rhotors)
     irhotors(1)=minloc(xx,1)
     Deffw=coeff_deff_ti * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_te*width)**2)
!    allocate(coretransp_out(it)%ti_transp%diff_eff(nrho,1))
     coretransp_out(it)%ti_transp%diff_eff(:,1) = deffin + Deffw
     ! ni
!    print *,'coretransp_in(it)%ni_transp%diff_eff(1,1,:)',coretransp_in(it)%ni_transp%diff_eff(1,1,:)
!    deffin = coretransp_in(it)%ni_transp%diff_eff(:,1,3)
     xx=abs(coretransp_in(it)%rho_tor(:)-rhotors)
     irhotors(1)=minloc(xx,1)
     Deffw=coeff_deff_ni * deffin(irhotors(1)) * exp(-xx**2/(coeff_width_ne*width)**2)
!    allocate(coretransp_out(it)%ni_transp%diff_eff(nrho,1,3))
     coretransp_out(it)%ni_transp%diff_eff(:,1,3) = deffin + Deffw

     print *, 'end of subroutine it',it

! end do
     
end subroutine ntm_Deff
