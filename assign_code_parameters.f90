subroutine assign_code_parameters(codeparameters, return_status)

  !-----------------------------------------------------------------------
  ! calls the XML parser for the code parameters and assign the
  ! resulting values to the corresponding variables
  !TODO: check an alternative and more elegant solution in Perl
  !-----------------------------------------------------------------------

  use itm_types
  use coeffs_module

  use euitm_schemas
  use euitm_xml_parser  

  IMPLICIT NONE

  type (type_param), intent(in) :: codeparameters
  integer(ITM_I4), intent(out) :: return_status 

  type(tree) :: parameter_list
  type(element), pointer :: temp_pointer
  integer(ITM_I4) :: i, nparm, n_values
  character(len = 132) :: cname

  return_status = 0      ! no error

  !-- parse xml-string codeparameters%parameters

  call euitm_xml_parse(codeparameters, nparm, parameter_list)
  !  print *,'codeparameters%parameters= ',codeparameters%parameters
  print *,' nparm= ',nparm
  !  print *,'parameter_list= ',parameter_list

  !-- assign variables

  temp_pointer => parameter_list%first

  outer: do
     cname = char2str(temp_pointer%cname)   ! necessary for AIX
     ! print *,'cname = ', trim(cname)
     select case (cname)
     case ("parameters")
        temp_pointer => temp_pointer%child
        cycle

     case ("coeff_deff_te")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, coeff_deff_te)
        ! print *,' coeff_deff_te= ',coeff_deff_te
     case ("coeff_deff_ne")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, coeff_deff_ne)
        ! print *,' coeff_deff_ne= ',coeff_deff_ne
     case ("coeff_deff_ti")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, coeff_deff_ti)
        ! print *,' coeff_deff_ti= ',coeff_deff_ti
     case ("coeff_deff_ni")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, coeff_deff_ni)
        ! print *,' coeff_deff_ni= ',coeff_deff_ni
     case ("coeff_deff_other")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, coeff_deff_other)
        ! print *,' coeff_deff_other= ',coeff_deff_other
     case ("coeff_width_te")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, coeff_width_te)
        ! print *,' coeff_width_te= ',coeff_width_te
     case ("coeff_width_ne")
        ! integer/real constant
        if (allocated(temp_pointer%cvalue)) &
             call char2num(temp_pointer%cvalue, coeff_width_ne)
        ! print *,' coeff_width_ne= ',coeff_width_ne

     case default
        write(*, *) 'ERROR: invalid parameter', cname
        return_status = 1
        !       exit
     end select
     do
        if (associated(temp_pointer%sibling)) then
           temp_pointer => temp_pointer%sibling
           exit
        end if
        if (associated(temp_pointer%parent, parameter_list%first )) &
             exit outer
        if (associated(temp_pointer%parent)) then
           temp_pointer => temp_pointer%parent
        else
           write(*, *) 'ERROR: broken list.'
           return
        end if
     end do
  end do outer

  !-- destroy tree
  call destroy_xml_tree(parameter_list)

  return

end subroutine assign_code_parameters
